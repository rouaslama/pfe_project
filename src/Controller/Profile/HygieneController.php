<?php


namespace App\Controller\Profile;


use App\Form\EditProfileType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/hygieneOfficer")
 * @Security("is_granted('ROLE_HYGIENE_OFFICER')")
 */

class HygieneController extends AbstractController
{
    /**
     * @Route("/Profile",name="profileAgent")
     */
    public function Profile(): Response
    {
        return $this->render('ProfilUsers/profile.html.twig');

    }

        /**
         * @Route("/edit",name="editProfileAgent")
         * Method({"GET", "POST"})
         */

        public function edit(Request $request)
    {   $agent=$this->getUser();
        $form = $this->createForm(EditProfileType::class, $agent);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            return $this->redirectToRoute('profileAgent');
        }
        return $this->render('ProfilUsers/editProfile.html.twig', ['form' => $form->createView()]);
    }


}