<?php


namespace App\Controller\Profile;


use App\Form\EditProfileType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/student")
 * @Security("is_granted('ROLE_STUDENT')")
 */
class StudentController extends AbstractController
{
    /**
     * @Route("/Profile",name="profileStudent")
     */
    public function Profile(): Response
    {
        return $this->render('ProfilUsers/profile.html.twig');

    }

    /**
     * @Route("/edit",name="editProfileStudent")
     * Method({"GET", "POST"})
     */

    public function edit(Request $request)
    {   $student=$this->getUser();
        $form = $this->createForm(EditProfileType::class, $student);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            return $this->redirectToRoute('profileStudent');
        }
        return $this->render('ProfilUsers/editProfile.html.twig', ['form' => $form->createView()]);
    }


}