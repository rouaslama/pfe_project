<?php


namespace App\Controller\Profile;


use App\Form\EditProfileType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/professor")
 * @Security("is_granted('ROLE_PROFESSOR')")
 */
class ProfController extends AbstractController
{
    /**
     * @Route("/Profile",name="profileProf")
     */
    public function Profile(): Response
    {
        return $this->render('ProfilUsers/profile.html.twig');

    }

    /**
     * @Route("/edit",name="editProfileProf")
     * Method({"GET", "POST"})
     */

    public function edit(Request $request)
    {   $prof=$this->getUser();
        $form = $this->createForm(EditProfileType::class, $prof);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            return $this->redirectToRoute('profileProf');
        }
        return $this->render('ProfilUsers/editProfile.html.twig', ['form' => $form->createView()]);
    }
}