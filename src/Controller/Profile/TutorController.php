<?php


namespace App\Controller\Profile;


use App\Form\EditProfileType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tutor")
 * @Security("is_granted('ROLE_TUTOR')")
 */
class TutorController extends AbstractController
{
    /**
     * @Route("/Profile",name="profileTutor")
     */
    public function Profile(): Response
    {
        return $this->render('ProfilUsers/profile.html.twig');

    }

    /**
     * @Route("/edit",name="editProfileTut")
     * Method({"GET", "POST"})
     */

    public function edit(Request $request)
    {   $tutor=$this->getUser();
        $form = $this->createForm(EditProfileType::class, $tutor);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            return $this->redirectToRoute('profileTutor');
        }
        return $this->render('ProfilUsers/editProfile.html.twig', ['form' => $form->createView()]);
    }

}