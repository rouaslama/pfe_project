<?php


namespace App\Controller;


use App\Services\PasswordGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestController
{
    /**
     * @Route("/test",name="test")
     */

    public function mdp(PasswordGenerator $passwordGenerator): Response
    {
        dd($passwordGenerator->generateRandomStrongPassword(8));
    }
}