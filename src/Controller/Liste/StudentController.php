<?php


namespace App\Controller\Liste;


use App\Entity\Professor;
use App\Entity\Seance;
use App\Entity\Student;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/student")
 * @Security("is_granted('ROLE_STUDENT')")
 */
class StudentController extends AbstractController
{
    /**
     * @Route("/listeProf", name="liste_Prof(student)")
     */
    public function showlisteProf() {

        $student=$this->getUser();
        $groupes=$student->getGroupe();
        $niveau=$groupes->getNiveau();
        $matieres=$niveau->getMatieres();
        $professeurs=[];
        foreach ($matieres as $mt){
            foreach ($mt->getProfessors() as $prof)
                array_push($professeurs, $prof);}
        return $this->render('liste_professor.html.twig', array('enseignant' => $professeurs));
}

    /**
     * @Route("/matiere", name="matiereStudent")
     */
    public function showMatiere() {

        $student=$this->getUser();
        $groupes=$student->getGroupe();
        $niveau=$groupes->getNiveau();
        $matieres=[];
        foreach ($niveau as $nv){
            foreach ($nv->getMatieres() as $matiere)
                array_push($matieres, $matiere);}
        return $this->render('matiere.html.twig', array('matiere' => $matieres));
    }
}