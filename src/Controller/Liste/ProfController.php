<?php


namespace App\Controller\Liste;


use App\Entity\Groupe;
use App\Entity\Note;
use App\Entity\Professor;
use App\Entity\Student;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/professor")
 * @Security("is_granted('ROLE_PROFESSOR')")
 */

class ProfController extends AbstractController
{
    /**
     * @Route("/listeEleve", name="liste_eleve(Prof)")
     */
    public function showlisteEleve() {

        $professor=$this->getUser();
        $matiere=$professor->getMatieres();
        foreach ($matiere as $mat)
        $niveau=$mat->getNiveaux();
        foreach ($niveau as $nv){
        $grp=$nv->getGroupes();}
        $tabGroupes=[];
        foreach ($grp as $groupe){
            foreach ($groupe->getStudents() as $student)
            array_push($tabGroupes, $student);}

        return $this->render('liste_eleve.html.twig', array('user' => $tabGroupes));
    }
    /**
     * @Route("/addNotes", name="add_notes(prof)")
     */
    public function addNotes() {

        $professor=$this->getUser();
        $matiere=$professor->getMatieres();
        foreach ($matiere as $mat)
            $niveau=$mat->getNiveaux();
        foreach ($niveau as $nv){
            $grp=$nv->getGroupes();}
        $tabGroupes=[];
        foreach ($grp as $groupe){
            foreach ($groupe->getStudents() as $student)
                array_push($tabGroupes, $student);}

        return $this->render('notes.html.twig', array( 'user'=>$tabGroupes));
    }

    /**
     * @Route("/note/new", name="new_note")
     * Method({"GET", "POST"})
     */
    public function new(Request $request) {
        $note = new Note();
        $form = $this->createFormBuilder($note)
            ->add('Devoir', TextType::class)
            ->add('periode', TextType::class)
            ->add('Note', TextType::class)->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $note = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($note);
            $entityManager->flush();

            return $this->redirectToRoute('new_note');
        }
        return $this->render('notes.html.twig',['form' => $form->createView()]);
    }

}