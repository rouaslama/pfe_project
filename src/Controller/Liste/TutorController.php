<?php


namespace App\Controller\Liste;


use App\Entity\Absence;
use App\Entity\Matiere;
use App\Entity\Note;
use App\Entity\Professor;
use App\Entity\Student;
use App\Entity\Tutor;
use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tutor")
 * @Security("is_granted('ROLE_TUTOR')")
 */
class TutorController extends AbstractController
{
    /**
     * @Route("/listeEleve", name="liste_eleve(tutor)")
     */
    public function showlisteEleve() {
        $tutor=$this->getUser();
        $user = $this->getDoctrine()->getRepository(Student::class)->findBy(["tutor" =>$tutor]);
        return $this->render('liste_eleve.html.twig', array('user' => $user));
    }


    /**
     * @Route("/listeProf", name="liste_Prof(tutor)")
     */
    public function showlisteProf() {
        $tutor=$this->getUser();
        $eleves = $this->getDoctrine()->getRepository(Student::class)->findBy(["tutor" =>$tutor]);
        return $this->render('liste_professor.html.twig', array( 'eleves'=>$eleves));
    }


    /**
     * @Route("/listeAbsence", name="liste_absence(tutor)")
     */
    public function showlisteAbsence() {

        $tutor=$this->getUser();
        $eleves = $this->getDoctrine()->getRepository(Student::class)->findBy(["tutor" =>$tutor]);
        return $this->render('absence.html.twig', array('eleves' => $eleves));
    }
    /**
     * @Route("/listeNotes", name="liste_notes(tutor)")
     */
    public function showlisteNotes() {

        $tutor=$this->getUser();
        $eleves = $this->getDoctrine()->getRepository(Student::class)->findBy(["tutor" =>$tutor]);

        return $this->render('notes.html.twig', array( 'eleves'=>$eleves));
    }

    /**
     * @Route("/listeMatieres", name="liste_matieres(tutor)")
     */
    public function showlisteMatieres() {

        $tutor=$this->getUser();
        $user = $this->getDoctrine()->getRepository(Student::class)->findBy(["tutor" =>$tutor]);

        return $this->render('matiere.html.twig', array('user' => $user));
    }
}