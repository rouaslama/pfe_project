<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;



class UserController extends AbstractController
{
    /**
     * @Route("/student",name="studentDashboard")
     */
    public function studentIndex(): Response
    {
        return $this->render('student.html.twig');

    }

    /**
     * @Route("/professor",name="professorDashboard")
     */
    public function professorIndex(): Response
    {
        return $this->render('professor.html.twig');

    }

    /**
     * @Route("/tutor",name="tutorDashboard")
     */
    public function tutorIndex(): Response
    {
        return $this->render('tutor.html.twig');

    }


    /**
     * @Route("/hygieneOfficer",name="hygOfficerDashboard")
     */
    public function hygOfficerIndex(): Response
    {
        return $this->render('hygiene_officer.html.twig');

    }




}