<?php

namespace App\Controller\Admin;

use App\Entity\Devoir;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TimeField;

class DevoirCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Devoir::class;
    }
    public function configureActions(Actions $actions): Actions
    {
        $detailUser=Action::new('detailUser','Detail','fa fa-eye')
            ->linkToCrudAction(Crud::PAGE_DETAIL)
            ->addCssClass('btn btn-outline-primary');




        return $actions
            ->setPermission(Action::DELETE,'ROLE_ADMIN')
            # ->disable(Action::EDIT)
            ->add(Crud::PAGE_INDEX,$detailUser)
            ->update(crud::PAGE_INDEX,Action::NEW,function(Action $action){
                return $action->setIcon('fa fa-user')->addCssClass('btn btn-warning');
            })
            ->update(crud::PAGE_INDEX,Action::EDIT,function(Action $action){
                return $action->setIcon('fa fa-edit')->addCssClass('btn btn-outline-success');
            })
            ->update(crud::PAGE_INDEX,Action::DELETE,function(Action $action) {
                return $action->setIcon('fa fa-trash')->addCssClass('btn btn-outline-danger');
            });

    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            AssociationField::new('type','type de devoir'),
            AssociationField::new('matiere','matière'),
            AssociationField::new('groupes'),
            DateField::new('date'),
            TimeField::new('hDebut','heure de début'),
            TimeField::new('hFin','heure de fin'),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            // the argument must be either one of these strings: 'short', 'medium', 'long', 'full', 'none'
            // (the strings are also available as \EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField::FORMAT_* constants)
            // or a valid ICU Datetime Pattern (see http://userguide.icu-project.org/formatparse/datetime)
            ->setTimeFormat('HH:mm')

            // first argument = datetime pattern or date format; second optional argument = time format
//            ->setDateTimeFormat('...', '...')

//            ->setDateIntervalFormat('%%y Year(s) %%m Month(s) %%d Day(s)')
//            ->setTimezone('...')

            // used to format numbers before rendering them on templates
//            ->setNumberFormat('%.2d');
            ;
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('id')
            ->add('type')
            ->add('matiere')
            ->add('groupes')
            ->add('date')
            ->add('hDebut')
            ->add('hFin')
            ;
    }


}
