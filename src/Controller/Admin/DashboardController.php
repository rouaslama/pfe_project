<?php

namespace App\Controller\Admin;
use App\Entity\Absence;
use App\Entity\Actualite;
use App\Entity\AnneeScolaire;
use App\Entity\Bulletin;
use App\Entity\Contact;
use App\Entity\Devoir;
use App\Entity\Document;
use App\Entity\Frais;
use App\Entity\Groupe;
use App\Entity\HygieneOfficer;
use App\Entity\Matiere;
use App\Entity\Message;
use App\Entity\Niveau;
use App\Entity\Note;
use App\Entity\Periode;
use App\Entity\Professor;
use App\Entity\Punition;
use App\Entity\Reglement;
use App\Entity\Salle;
use App\Entity\Seance;
use App\Entity\Tranche;
use App\Entity\Tutor;
use App\Entity\Student;
use App\Entity\Type;
use App\Entity\TypePunition;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="adminDashboard")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function index(): Response
    {
        return $this->render('bundles/EasyAdminBundle/welcome.html.twig',[
            'user' =>[]
        ]);

    }
    public function configureAssets(): Assets
    {
        return Assets::new()
            ->addCssFile('bundle/easyadmin/css/style.css');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
        -> setTitle('PascalSchool')

            ;

    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linktoDashboard('Dashboard', 'fa fa-home'),
            MenuItem::linkToCrud('élèves', 'fa fa-child', Student::class),
            MenuItem::linkToCrud('Tuteurs', 'fa fa-user-o', Tutor::class),
            MenuItem::linkToCrud('Enseignants', 'fa fa-user-circle-o', Professor::class),
            MenuItem::linkToCrud("Agents d'hygiènes", 'fa fa-heartbeat', HygieneOfficer::class),

            MenuItem::subMenu("Préparation d'année", 'fa fa-sitemap')->setSubItems([
                MenuItem::linkToCrud('Années Scolaire', 'fa fa-sitemap', AnneeScolaire::class),
                MenuItem::linkToCrud('Périodes', 'fa fa-level-up', Periode::class),
                MenuItem::linkToCrud('Niveaux', 'fa fa-sitemap', Niveau::class),
                MenuItem::linkToCrud('Groupes', 'fa fa-level-up', Groupe::class),
                MenuItem::linkToCrud('salles', 'fa fa-university', Salle::class),
            ]),

            MenuItem::linkToCrud('Matières', 'fa fa-file-text', Matiere::class),
            MenuItem::linkToCrud('Seances', 'fa fa-list-alt', Seance::class),
            MenuItem::subMenu('Devoirs', 'fa fa-newspaper-o')->setSubItems([
                MenuItem::linkToCrud('Types Devoirs', 'fa fa-newspaper-o', Type::class),
                MenuItem::linkToCrud('Devoirs', 'fa fa-newspaper-o', Devoir::class),
            ]),
            MenuItem::subMenu('Notes', 'fa fa-file-text')->setSubItems([
                MenuItem::linkToCrud('Notes', 'fa fa-file-text', Note::class),
                MenuItem::linkToCrud('Bulletins', 'fa fa-file-text', Bulletin::class),
            ]),

                MenuItem::linkToCrud('Absences', 'fa fa-save', Absence::class),
                MenuItem::subMenu('Punitions', 'fa fa-save')->setSubItems([
                    MenuItem::linkToCrud('Types Punitions', 'fa fa-save', TypePunition::class),
                    MenuItem::linkToCrud('Punitions', 'fa fa-save', Punition::class),
                ]),


//            MenuItem::linkToCrud('Absences', 'fa fa-save', Absence::class),
//            MenuItem::subMenu('Punitions', 'fa fa-save')->setSubItems([
//                MenuItem::linkToCrud('Types Punitions', 'fa fa-save', TypePunition::class),
//                MenuItem::linkToCrud('Punitions', 'fa fa-save', Punition::class),
//            ]),


            MenuItem::subMenu('Paiement', 'fa fa-dollar')->setSubItems([
            MenuItem::linkToCrud('Catégories de Frais', 'fa fa-dollar', Frais::class),
            MenuItem::linkToCrud('Tranches', 'fa fa-dollar', Tranche::class),
            MenuItem::linkToCrud('Reglement', 'fa fa-dollar', Reglement::class),
        ]),

            MenuItem::linkToCrud('Bibliothèque', 'fa fa-book', Document::class),
            MenuItem::linkToCrud('Actualités', 'fa fa-bullhorn', Actualite::class),

            MenuItem::linkToCrud('Message', 'fa fa-envelope-open-o', Message::class),
        ];

    }

}