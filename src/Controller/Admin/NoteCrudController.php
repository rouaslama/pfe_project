<?php

namespace App\Controller\Admin;

use App\Entity\Note;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class NoteCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Note::class;
    }
    public function configureActions(Actions $actions): Actions
    {
        $detailUser=Action::new('detailUser','Detail','fa fa-eye')
            ->linkToCrudAction(Crud::PAGE_DETAIL)
            ->addCssClass('btn btn-outline-primary');




        return $actions

            ->disable(Action::EDIT)
            ->disable(Action::DELETE)
            ->disable(Action::NEW)
            ->add(Crud::PAGE_INDEX,$detailUser)
//            ->update(crud::PAGE_INDEX,Action::NEW,function(Action $action){
//                return $action->setIcon('fa fa-user')->addCssClass('btn btn-warning');
//            })
//            ->update(crud::PAGE_INDEX,Action::EDIT,function(Action $action){
//                return $action->setIcon('fa fa-edit')->addCssClass('btn btn-outline-success');
//            })
//            ->update(crud::PAGE_INDEX,Action::DELETE,function(Action $action) {
//                return $action->setIcon('fa fa-trash')->addCssClass('btn btn-outline-danger');
//            })
            ;

    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            AssociationField::new('student','student'),
            NumberField::new('note'),
            AssociationField::new('devoir'),
            TextField::new('appreciation'),
            AssociationField::new('periode'),
        ];
    }

}
