<?php

namespace App\Controller\Admin;

use App\Entity\Absence;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class AbsenceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Absence::class;
    }
    public function configureActions(Actions $actions): Actions
    {
        $detailUser=Action::new('detailUser','Detail','fa fa-eye')
            ->linkToCrudAction(Crud::PAGE_DETAIL)
            ->addCssClass('btn btn-outline-primary');




        return $actions
            ->remove(Crud::PAGE_INDEX, Action::EDIT)
            ->remove(Crud::PAGE_INDEX, Action::DELETE)
            ->remove(Crud::PAGE_INDEX, Action::NEW)
//            ->disable(Action::EDIT)
//            ->disable(Action::DELETE)
//            ->disable(Action::NEW)
            ->add(Crud::PAGE_INDEX,$detailUser)
//            ->update(crud::PAGE_INDEX,Action::NEW,function(Action $action){
//                return $action->setIcon('fa fa-user')->addCssClass('btn btn-warning');
//            })
//            ->update(crud::PAGE_INDEX,Action::EDIT,function(Action $action){
//                return $action->setIcon('fa fa-edit')->addCssClass('btn btn-outline-success');
//            })
//            ->update(crud::PAGE_INDEX,Action::DELETE,function(Action $action) {
//                return $action->setIcon('fa fa-trash')->addCssClass('btn btn-outline-danger');
//            })
            ;

    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            AssociationField::new('students','élèves'),
            AssociationField::new('seance','seance'),
            ChoiceField::new('type', "type  d'absence")
                ->allowMultipleChoices(false)
                ->autocomplete()
                ->setChoices(['léagle' => 'légale',
                    'non léagle' =>'non légale']),


        ];
    }

}
