<?php

namespace App\Controller\Admin;


use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use App\Field\VichImageField;
use App\Entity\Student;
use Vich\UploaderBundle\Form\Type\VichImageType;

class StudentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Student::class;
    }
    public function configureActions(Actions $actions): Actions
    {
        $detailUser=Action::new('detailUser','Detail','fa fa-eye')
            ->linkToCrudAction(Crud::PAGE_DETAIL)
            ->addCssClass('btn btn-outline-primary');




        return $actions
            ->setPermission(Action::DELETE,'ROLE_ADMIN')
            # ->disable(Action::EDIT)
            ->add(Crud::PAGE_INDEX,$detailUser)
            ->update(crud::PAGE_INDEX,Action::NEW,function(Action $action){
                return $action->setIcon('fa fa-user')->addCssClass('btn btn-warning');
            })
            ->update(crud::PAGE_INDEX,Action::EDIT,function(Action $action){
                return $action->setIcon('fa fa-edit')->addCssClass('btn btn-outline-success');
            })
            ->update(crud::PAGE_INDEX,Action::DELETE,function(Action $action) {
                return $action->setIcon('fa fa-trash')->addCssClass('btn btn-outline-danger');
            });

    }



    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'ID')
                ->onlyOnIndex(),
            TextField::new('userName','login')->hideOnForm()->hideOnDetail()->hideOnIndex(),
            TextField::new('password','Mot de passe')->hideOnForm()->hideOnDetail()->hideOnIndex(),
            ArrayField::new('roles', 'Role')->hideOnForm()->hideOnIndex()->hideOnDetail(),


            TextField::new('firstName','Prénom'),
            TextField::new('lastName','Nom'),
            DateField::new('birthday','Date de naissance'),
            ChoiceField::new('gender', 'Sexe')
                ->allowMultipleChoices(false)
                ->autocomplete()
                ->setChoices(['Femme' => 'Femme',
                    'Homme' =>'Homme'])->hideOnIndex(),
            TextField::new('address','Adresse')->hideOnIndex(),
            EmailField::new('email')->onlyOnDetail(),
            TelephoneField::new('tel','Téléphone'),
            TextareaField::new('photoFile','Photo')
                ->setFormType(VichImageType::class)->onlyOnForms(),
            ImageField::new('photo')
                ->setBasePath('uploads/files') ->setUploadDir('public/uploads/files')
                ->setLabel('Image')->onlyOnDetail(),
            AssociationField::new('tutor','Tuteur')->hideOnIndex(),
            AssociationField::new('groupe',"Groupe"),
//            DateTimeField::new('updatedAt','mis a jour le ')

        ];
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('firstName')
            ->add('lastName')
            ->add('groupe')
            ->add('tutor')
//            ->add('updatedAt')
            ;
    }

}
