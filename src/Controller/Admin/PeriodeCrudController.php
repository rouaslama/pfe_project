<?php

namespace App\Controller\Admin;

use App\Entity\Periode;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;

class PeriodeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Periode::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        $detailUser=Action::new('detailUser','Detail','fa fa-eye')
            ->linkToCrudAction(Crud::PAGE_DETAIL)
            ->addCssClass('btn btn-outline-primary');




        return $actions
            ->setPermission(Action::DELETE,'ROLE_ADMIN')
            # ->disable(Action::EDIT)
            ->add(Crud::PAGE_INDEX,$detailUser)
            ->update(crud::PAGE_INDEX,Action::NEW,function(Action $action){
                return $action->setIcon('fa fa-user')->addCssClass('btn btn-warning');
            })
            ->update(crud::PAGE_INDEX,Action::EDIT,function(Action $action){
                return $action->setIcon('fa fa-edit')->addCssClass('btn btn-outline-success');
            })
            ->update(crud::PAGE_INDEX,Action::DELETE,function(Action $action) {
                return $action->setIcon('fa fa-trash')->addCssClass('btn btn-outline-danger');
            });

    }


    public function configureFields(string $pageName): iterable
    {
        /*
        //getAnnees() fnc declared in AnneeRepository
        $anneesPasClot = $this->getDoctrine()->getRepository(\App\Entity\AnneeScolaire::class)->findBy([
            "cloturee" => false
        ]);
//        dd($anneesPasClot);
        $libAnnees = [];
        foreach ($anneesPasClot as $annee){
            $libAnn = $annee->getLibelle();

            array_push($libAnnees,$libAnn);
        }
//        dd($libAnnees);

        $anneesArray = array_column($libAnnees, 'anneeScolaire');
//        dd($anneesArray);
        $anneesResult = [];
        foreach ($anneesArray as $key =>$val){
            $anneesResult[$val] = $val;
        }
//        dd($anneesResult);
        */
        return [
            IdField::new('id')->hideOnForm(),
//            IntegerField::new('numPeriode','Numero de periode'),
            ChoiceField::new('numPeriode','Numero de periode')
                ->allowMultipleChoices(false)
                ->autocomplete()
                ->setChoices(['1' => '1',
                              '2' =>'2',
                              '3' =>'3' ]),
            // date debut, date fin -->subscriber : doit avoir la meme annee scolaire prepare,
            AssociationField::new('anneeScolaire','Année scolaire'),
                /*
            ChoiceField::new('anneeScolaire')
                ->allowMultipleChoices(false)
                ->autocomplete()
                ->setChoices($anneesResult),
                */
            BooleanField::new('cloturee',' Periode cloturée'),
            DateField::new('dateDebut', 'date de début')->hideOnIndex(),
            DateField::new('dateFin', 'date de fin')->hideOnIndex(),

        ];
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('id')
            ->add('numPeriode')
            ->add('anneeScolaire')
            ->add('cloturee')
            ;
    }


}
