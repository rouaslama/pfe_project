<?php

namespace App\Controller\Admin;

use App\Entity\Tutor;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use PhpParser\Node\Expr\New_;
use Vich\UploaderBundle\Form\Type\VichImageType;

class TutorCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Tutor::class;
    }
    public function configureActions(Actions $actions): Actions
    {
        $detailUser=Action::new('detailUser','Detail','fa fa-eye')
            ->linkToCrudAction(Crud::PAGE_DETAIL)
            ->addCssClass('btn btn-outline-primary');

        /////////////// btn supprimer //////////////
        $Resetpwd=Action::new('Resetpwd','Reset','')
            ->linkToCrudAction(Crud::PAGE_EDIT)
            // ->displayAsLink()
            ->displayAsButton()
            ->addCssClass('btn btn-outline-primary');



        return $actions
            ->setPermission(Action::DELETE,'ROLE_ADMIN')
            # ->disable(Action::EDIT)
            ->add(Crud::PAGE_INDEX,$detailUser)

//            ->add(Crud::PAGE_EDIT,$Resetpwd)->update(Crud::PAGE_EDIT,Action::NEW,)


            ->update(crud::PAGE_INDEX,Action::NEW,function(Action $action){
                return $action->setIcon('fa fa-user')->addCssClass('btn btn-warning');
            })
            ->update(crud::PAGE_INDEX,Action::EDIT,function(Action $action){
                return $action->setIcon('fa fa-edit')->addCssClass('btn btn-outline-success');
            })
            ->update(crud::PAGE_INDEX,Action::DELETE,function(Action $action) {
                return $action->setIcon('fa fa-trash')->addCssClass('btn btn-outline-danger');


            });

    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'ID')
                ->onlyOnIndex(),
            IntegerField::new('cin'),
            EmailField::new('email'),
            TextField::new('userName','login')->hideOnForm()->hideOnIndex()->hideOnDetail(),
            TextField::new('password','Mot de passe')->hideOnForm()->hideOnIndex()->hideOnDetail(),
            ArrayField::new('roles', 'Role')->hideOnForm()->hideOnIndex()->hideOnDetail(),
            TextField::new('lastName','Nom'),
            TextField::new('firstName','Prénom'),
            DateField::new('birthday','Date de naissance')->hideOnIndex(),
            ChoiceField::new('gender', 'sexe')
                ->allowMultipleChoices(false)
                ->autocomplete()
                ->setChoices(['Femme' => 'femme',
                    'Homme' =>'homme'])->hideOnIndex(),
            TextField::new('address','Adresse')->hideOnIndex(),
            TelephoneField::new('tel','Téléphone'),
            TextareaField::new('photoFile','Photo')
                ->setFormType(VichImageType::class)->onlyOnForms(),
            ImageField::new('photo')->onlyOnDetail()
                ->setBasePath('uploads/files') ->setUploadDir('public/uploads/files')
                ->setLabel('Image'),
//            AssociationField::new('students','élèves correspondants')
        ];
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('userName');
    }
}
