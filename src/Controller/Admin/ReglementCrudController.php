<?php

namespace App\Controller\Admin;

use App\Entity\Reglement;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ReglementCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Reglement::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        $detailUser=Action::new('detailUser','Detail','fa fa-eye')
            ->linkToCrudAction(Crud::PAGE_DETAIL)
            ->addCssClass('btn btn-outline-primary');

        return $actions
            ->setPermission(Action::DELETE,'ROLE_ADMIN')
            // ->disable(Action::EDIT)
            ->add(Crud::PAGE_INDEX,$detailUser)
            ->update(crud::PAGE_INDEX,Action::NEW,function(Action $action){
                return $action->setIcon('fa fa-dollar')->addCssClass('btn btn-warning');
            })
            ->update(crud::PAGE_INDEX,Action::EDIT,function(Action $action){
                return $action->setIcon('fa fa-edit')->addCssClass('btn btn-outline-success');
            })
//            ->disable(Action::DELETE)
            ;

//            ->update(crud::PAGE_INDEX,Action::DELETE,function(Action $action) {
//                return $action->setIcon('fa fa-trash')->addCssClass('btn btn-outline-danger');
//            });

    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            AssociationField::new('student','élève'),
            AssociationField::new('frais','categorie de Frais'),
            AssociationField::new('tranche'),
            TextField::new('statusFrais','Status de frais')->hideOnForm(),
            DateTimeField::new('dateReglement','date de réglement')->hideOnForm()->hideOnIndex(),
            TextField::new('modeRegl','mode de reglement')->hideOnForm(),

        ];
    }
    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('student')
            ->add('frais')
            ->add('tranche')
            ->add('statusFrais')
            ->add('dateReglement')
            ->add('modeRegl')
            ;
    }

}
