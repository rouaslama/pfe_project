<?php

namespace App\Repository;

use App\Entity\Professor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Professor|null find($id, $lockMode = null, $lockVersion = null)
 * @method Professor|null findOneBy(array $criteria, array $orderBy = null)
 * @method Professor[]    findAll()
 * @method Professor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProfessorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Professor::class);
    }

     /**
      * @return Professor[] Returns an array of Professor objects
      */
    public function findByExampleField($val)
    {
        return $this->createQueryBuilder('s')
            ->Join('s.groupe', 'p')
            ->Join('p.groupes', 'c')
            ->andWhere('c.groupes = :groupes')
            ->setParameter('c.groupes', $val)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
            
//        $qb = $this->createQueryBuilder('s');
//        $qb ->join('a.Student', 'p')
//            ->where($qb->expr()->in('c.Professor', $nom_categories));
//        return $qb->getQuery()
//            ->getResult();
        ;
    }

    /*
    public function findOneBySomeField($value): ?Professor
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
