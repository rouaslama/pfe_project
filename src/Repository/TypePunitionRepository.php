<?php

namespace App\Repository;

use App\Entity\TypePunition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypePunition|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypePunition|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypePunition[]    findAll()
 * @method TypePunition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypePunitionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypePunition::class);
    }

    // /**
    //  * @return TypePunition[] Returns an array of TypePunition objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypePunition
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
