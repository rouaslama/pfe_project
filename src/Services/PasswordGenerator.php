<?php


namespace App\Services;


class PasswordGenerator
{
    public function generateRandomStrongPassword(int $length): string
    {
        $uppercaseletters = $this->generateCharacterswithCharCodeRange([65,90]);

        $lowercaseletters = $this->generateCharacterswithCharCodeRange([97,122]);

        $numbers = $this->generateCharacterswithCharCodeRange([48,57]);

        $symbols = $this->generateCharacterswithCharCodeRange([33,47,58,64,91,96,123,126]);

        $allCharacters = array_merge($uppercaseletters,$lowercaseletters,$numbers,$symbols);

        $isArrayShuffled= shuffle($allCharacters);

        if(!$isArrayShuffled){
            throw new \LogicException("la génération du mot de passe aléatoire a échoué ,veuillez réessayer");
        }
        return implode('',array_slice($allCharacters,0,$length));
    }
    private function generateCharacterswithCharCodeRange(array $range): array
    {
        if(count($range) == 2){

            return range(chr($range[0]),chr($range[1]));

        }else{
            #  return array_merge(...array_map(fn ($range) => range(chr($range[0]), chr($range[1])), array_chunk($range,2));
            return array_merge(...array_map(function ($range)  {
                return range(chr($range[0]), chr($range[1]));
            }, array_chunk($range,2)));

        }

    }

}
