<?php


namespace App\DataFixtures;

use App\Entity\Administrator;
use App\Entity\Professor;
use App\Entity\Tutor;
use App\Entity\User;
use App\Entity\Student;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class UserFixtures extends Fixture
{

    public const ADMIN_USER_REFERENCE = 'Admin-user';
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $admin = new Administrator();
        $admin->setEmail('chadha.glaid@gmail.com');
        $admin->setUserName('admin');
        $admin->setRoles(["ROLE_ADMIN"]);
        $admin->setCin('12845633');
        $admin->setFirstName('Chadha');
        $admin->setLastName('Glaied');
        $admin->setBirthday(DateTime::createFromFormat('Y-m-d', "1988-08-18"));
        $admin->setGender('Femme');
        $admin->setAddress('Sousse,Rue de lenvironnement');
        $admin->setTel('21328284');
        $admin->setPhoto('');
        $admin->setUpdatedAt(new \DateTime('now'));
        //encodePassword
        $password = $this->encoder->encodePassword($admin, 'admin123');
        $admin->setPassword($password);
        //save the Administrator!
        $manager->persist($admin);
        $manager->flush();

        // other fixtures can get this object using the UserFixtures::ADMIN_USER_REFERENCE constant
        $this->addReference(self::ADMIN_USER_REFERENCE, $admin);


//
//        //**************************   Professor *****************************************
//
//        $user_Professor = new Professor();
//        $user_Professor->setEmail('rouaslama8@gmail.com');
//        $user_Professor->setUserName('roua');
//        $user_Professor->setRoles(["ROLE_POFESSOR"]);
//        $user_Professor->setCin('12845633');
//        $user_Professor->setFirstName('Roua');
//        $user_Professor->setLastName('Slama');
//        $user_Professor->setBirthday(DateTime::createFromFormat('Y-m-d', "1988-08-08"));
//        $user_Professor->setGender('Femme');
//        $user_Professor->setAddress('Sousse,Rue de bassatin');
//        $user_Professor->setTel('21328285');
//        $user_Professor->setPhoto('');
//        $user_Professor->setUpdatedAt(new \DateTime('now'));
//        //encodePassword
//        $password = $this->encoder->encodePassword($user_Professor, '12345678');
//        $user_Professor->setPassword($password);
//        //save the User!
//        $manager->persist($user_Professor);
//        $manager->flush();

//        //**************************************    Tutor ***************************************
//
//        $user_Professor = new Tutor();
//        $user_Professor->setEmail('tutor@gmail.com');
//        $user_Professor->setUserName('tutor');
//        $user_Professor->setRoles(["ROLE_TUTOR"]);
//        $user_Professor->setCin('03251487');
//        $user_Professor->setFirstName('tut');
//        $user_Professor->setLastName('tutoo');
//        $user_Professor->setBirthday(DateTime::createFromFormat('Y-m-d', "1987-12-07"));
//        $user_Professor->setGender('homme');
//        $user_Professor->setAddress('Sousse,Rue de bassatin');
//        $user_Professor->setTel('20365987');
//        $user_Professor->setPhoto('');
//        $user_Professor->setUpdatedAt(new \DateTime('now'));
//        //encodePassword
//        $password = $this->encoder->encodePassword($user_Professor, '00000000');
//        $user_Professor->setPassword($password);
//        //save the User!
//        $manager->persist($user_Professor);
//        $manager->flush();
//

    }

}