<?php

namespace App\Entity;

use App\Repository\GroupeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GroupeRepository::class)
 */
class Groupe
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbEleves;



    /**
     * @ORM\OneToMany(targetEntity=seance::class, mappedBy="groupe")
     */
    private $seance;


    /**
     * @ORM\ManyToOne(targetEntity=Niveau::class, inversedBy="groupes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $niveau;



    /**
     * @ORM\OneToMany(targetEntity=Student::class, mappedBy="groupe")
     */
    private $students;


    /**
     * @ORM\Column(type="integer")
     */
    private $nbMax;

    /**
     * @ORM\ManyToMany(targetEntity=Devoir::class, mappedBy="groupes")
     */
    private $devoirs;

    /**
     * @ORM\ManyToMany(targetEntity=Actualite::class, mappedBy="groupes")
     */
    private $actualites;

    /**
     * @ORM\ManyToMany(targetEntity=Document::class, mappedBy="groupes")
     */
    private $documents;



    public function __construct()
    {
        $this->seance = new ArrayCollection();
        $this->nbEleves = 0;
        $this->students = new ArrayCollection();
        $this->devoirs = new ArrayCollection();
        $this->actualites = new ArrayCollection();
        $this->documents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getNbEleves(): ?int
    {
        return $this->nbEleves;
    }

    public function setNbEleves(int $nbEleves): self
    {
        $this->nbEleves = $nbEleves;

        return $this;
    }



    /**
     * @return Collection|seance[]
     */
    public function getSeance(): Collection
    {
        return $this->seance;
    }

    public function addSeance(seance $seance): self
    {
        if (!$this->seance->contains($seance)) {
            $this->seance[] = $seance;
            $seance->setGroupe($this);
        }

        return $this;
    }

    public function removeSeance(seance $seance): self
    {
        if ($this->seance->removeElement($seance)) {
            // set the owning side to null (unless already changed)
            if ($seance->getGroupe() === $this) {
                $seance->setGroupe(null);
            }
        }

        return $this;
    }


    /**
     * @return Niveau|null
     */
    public function getNiveau(): ?Niveau
    {
        return $this->niveau;
    }

    /**
     * @param Niveau|null $niveau
     * @return $this
     */
    public function setNiveau(?Niveau $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }



    /**
     * @return Collection|Student[]
     */
    public function getStudents(): Collection
    {
        return $this->students;
    }

    public function addStudent(Student $student): self
    {
        if (!$this->students->contains($student)) {
            $this->students[] = $student;
            $student->setGroupe($this);
        }

        return $this;
    }

    public function removeStudent(Student $student): self
    {
        if ($this->students->removeElement($student)) {
            // set the owning side to null (unless already changed)
            if ($student->getGroupe() === $this) {
                $student->setGroupe(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getNiveau()->getLibelle().' '.$this->getLibelle().' ('.$this->getNiveau()->getAnneeScolaire().')' ;
    }



    public function getNbMax(): ?int
    {
        return $this->nbMax;
    }

    public function setNbMax(int $nbMax): self
    {
        $this->nbMax = $nbMax;

        return $this;
    }

    /**
     * @return Collection|Devoir[]
     */
    public function getDevoirs(): Collection
    {
        return $this->devoirs;
    }

    public function addDevoir(Devoir $devoir): self
    {
        if (!$this->devoirs->contains($devoir)) {
            $this->devoirs[] = $devoir;
        }

        return $this;
    }

    public function removeDevoir(Devoir $devoir): self
    {
        $this->devoirs->removeElement($devoir);

        return $this;
    }


    /**
     * @return Collection|Actualite[]
     */
    public function getActualites(): Collection
    {
        return $this->actualites;
    }

    public function addActualite(Actualite $actualite): self
    {
        if (!$this->actualites->contains($actualite)) {
            $this->actualites[] = $actualite;
            $actualite->addGroupe($this);
        }

        return $this;
    }

    public function removeActualite(Actualite $actualite): self
    {
        if ($this->actualites->removeElement($actualite)) {
            $actualite->removeGroupe($this);
        }

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->addGroupe($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->removeElement($document)) {
            $document->removeGroupe($this);
        }

        return $this;
    }


}
