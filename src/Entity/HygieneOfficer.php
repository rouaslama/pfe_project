<?php

namespace App\Entity;

use App\Repository\HygieneOfficerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=HygieneOfficerRepository::class)
 */
class HygieneOfficer extends User
{



    /**
     * @ORM\Column(type="integer", unique=true)
     * @Assert\Length(
     *     min = 8,
     *     max = 8,
     *      minMessage = "le num de CIN doit faire {{ limit }} chiffres",
     *      maxMessage = "le num de CIN doit faire {{ limit }} chiffres"
     * )
     */
    private $cin;



    /**
     * @ORM\ManyToMany(targetEntity=Actualite::class, mappedBy="hygieneOfficers")
     */
    private $actualites;

    public function __construct()
    {
        parent::__construct();
        $this->actualites = new ArrayCollection();
    }



    public function getCin(): ?int
    {
        return $this->cin;
    }

    public function setCin(int $cin): self
    {
        $this->cin = $cin;

        return $this;
    }

    public function __toString()
    {
        return '#'.$this->getId().' '.$this->getFirstName().' '.$this->getLastName();
    }


    /**
     * @return Collection|Actualite[]
     */
    public function getActualites(): Collection
    {
        return $this->actualites;
    }

    public function addActualite(Actualite $actualite): self
    {
        if (!$this->actualites->contains($actualite)) {
            $this->actualites[] = $actualite;
        }

        return $this;
    }

    public function removeActualite(Actualite $actualite): self
    {
        $this->actualites->removeElement($actualite);

        return $this;
    }
}
