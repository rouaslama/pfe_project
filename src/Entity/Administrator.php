<?php

namespace App\Entity;

use App\Repository\AdministratorRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=AdministratorRepository::class)
 */
class Administrator extends User implements UserInterface
{



    /**
     * @ORM\Column(type="integer", unique=true)
     * @Assert\Length(
     *     min = 8,
     *     max = 8,
     *      minMessage = "le num de CIN doit faire {{ limit }} chiffres",
     *      maxMessage = "le num de CIN doit faire {{ limit }} chiffres"
     * )
     */
    private $cin;



    public function getCin(): ?int
    {
        return $this->cin;
    }

    public function setCin(int $cin): self
    {
        $this->cin = $cin;

        return $this;
    }

    public function __toString()
    {
        return $this->getFirstName().' '.$this->getLastName();
    }
}
