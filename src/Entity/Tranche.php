<?php

namespace App\Entity;

use App\Repository\TrancheRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=TrancheRepository::class)
 */
class
Tranche
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity=Frais::class, inversedBy="tranches")
     * @ORM\JoinColumn(nullable=false)
     */
    private $frais;

    /**
     * @ORM\OneToMany(targetEntity=Reglement::class, mappedBy="tranche")
     * @ORM\JoinColumn(nullable=true)
     */
    private $reglements;

    /**
     * @var float|null
     * @ORM\Column(type="decimal", nullable=false, precision=12, scale=3, options={"default":0})
     * @Assert\Type(type="float", message = "The value {{ value }} must be of type {{ type }}")
     *
     */
    private $montant;

    /**
     * @ORM\Column(type="integer")
     */
    private $numTranche;





    public function __construct()
    {
        $this->reglements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


     public function getFrais(): ?Frais
     {
         return $this->frais;
     }

     public function setFrais(?Frais $frais): self
     {
         $this->frais = $frais;

         return $this;
     }

    /**
     * @return ArrayCollection
     */
    public function getReglements(): ArrayCollection
    {
        return $this->reglements;
    }

    /**
     * @param ArrayCollection $reglements
     * @return Tranche
     */
    public function setReglements(ArrayCollection $reglements): Tranche
    {
        $this->reglements = $reglements;
        return $this;
    }

    public function getMontant()
    {
        return $this->montant;
    }

    public function setMontant($montant): self
    {
        $this->montant = $montant;

        return $this;
    }



    public function getNumTranche(): ?int
    {
        return $this->numTranche;
    }

    public function setNumTranche(int $numTranche): self
    {
        $this->numTranche = $numTranche;

        return $this;
    }

    public function __toString()
    {
        return $this->getMontant().'->tranche n°'.$this->getNumTranche();//.'->'.$this->getFrais()->getLibelle();
    }


}
