<?php

namespace App\Entity;

use App\Repository\BulletinRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=BulletinRepository::class)
 */
class Bulletin
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\ManyToMany(targetEntity=Periode::class, mappedBy="bulletins")
     */
    private $periodes;

    /**
     * @var float|null
     * @ORM\Column(type="decimal", nullable=true, precision=4, scale=2)
     * @Assert\Type(type="float", message = "The value {{ value }} must be of type {{ type }}")
     *
     */
    private $moyenneG;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $appreciation;

    public function __construct()
    {
        $this->periodes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * @return Collection|Periode[]
     */
    public function getPeriodes(): Collection
    {
        return $this->periodes;
    }

    public function addPeriode(Periode $periode): self
    {
        if (!$this->periodes->contains($periode)) {
            $this->periodes[] = $periode;
            $periode->addBulletin($this);
        }

        return $this;
    }

    public function removePeriode(Periode $periode): self
    {
        if ($this->periodes->removeElement($periode)) {
            $periode->removeBulletin($this);
        }

        return $this;
    }

    /**
     * @return float|null
     */
    public function getMoyenneG(): ?float
    {
        return $this->moyenneG;
    }

    /**
     * @param float|null $moyenneG
     * @return Bulletin
     */
    public function setMoyenneG(?float $moyenneG): Bulletin
    {
        $this->moyenneG = $moyenneG;
        return $this;
    }



    public function getAppreciation(): ?string
    {
        return $this->appreciation;
    }

    public function setAppreciation(?string $appreciation): self
    {
        $this->appreciation = $appreciation;

        return $this;
    }
}
