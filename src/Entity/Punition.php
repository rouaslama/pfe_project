<?php

namespace App\Entity;

use App\Repository\PunitionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PunitionRepository::class)
 */
class Punition
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $raison;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateCreation;



    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $duree;

    /**
     * @ORM\ManyToMany(targetEntity=Student::class, inversedBy="punitions")
     */
    private $students;

    /**
     * @ORM\ManyToOne(targetEntity=TypePunition::class, inversedBy="punitions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $typePunition;

    public function __construct()
    {
        $this->students = new ArrayCollection();
        $this->dateCreation = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getRaison()
    {
        return $this->raison;
    }

    /**
     * @param mixed $raison
     * @return Punition
     */
    public function setRaison($raison)
    {
        $this->raison = $raison;
        return $this;
    }



    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(\DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }




    public function getDuree(): ?string
    {
        return $this->duree;
    }

    public function setDuree(?string $duree): self
    {
        $this->duree = $duree;

        return $this;
    }

    /**
     * @return Collection|Student[]
     */
    public function getStudents(): Collection
    {
        return $this->students;
    }

    public function addStudent(Student $student): self
    {
        if (!$this->students->contains($student)) {
            $this->students[] = $student;
        }

        return $this;
    }

    public function removeStudent(Student $student): self
    {
        $this->students->removeElement($student);

        return $this;
    }

    public function getTypePunition(): ?TypePunition
    {
        return $this->typePunition;
    }

    public function setTypePunition(?TypePunition $typePunition): self
    {
        $this->typePunition = $typePunition;

        return $this;
    }
}
