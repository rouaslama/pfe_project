<?php

namespace App\Entity;

use App\Repository\MatiereRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=MatiereRepository::class)
 */
class Matiere
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;


    /**
     * @ORM\OneToMany(targetEntity=Devoir::class, mappedBy="matiere")
     */
    private $devoirs;

    /**
     * @var float|null
     * @ORM\Column(type="decimal", nullable=false, precision=4, scale=2)
     * @Assert\Type(type="float", message = "The value {{ value }} must be of type {{ type }}")
     *
     */
    private $coefficient;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $moyenneM;



    /**
     * @ORM\OneToMany(targetEntity=seance::class, mappedBy="matiere")
     */
    private $seances;


    /**
     * @ORM\ManyToMany(targetEntity=Professor::class, mappedBy="matieres")
     */
    private $professors;

    /**
     * @ORM\ManyToMany(targetEntity=Niveau::class, inversedBy="matieres")
     */
    private $niveaux;


    public function __construct()
    {
        $this->devoirs = new ArrayCollection();
        $this->seances = new ArrayCollection();
        $this->professors = new ArrayCollection();
        $this->niveaux = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }


    /**
     * @return Collection|Devoir[]
     */
    public function getDevoirs(): Collection
    {
        return $this->devoirs;
    }

    public function addDevoir(Devoir $devoir): self
    {
        if (!$this->devoirs->contains($devoir)) {
            $this->devoirs[] = $devoir;

            $devoir->setMatiere($this);
        }

        return $this;
    }

    public function removeDevoir(Devoir $devoir): self
    {
        if ($this->devoirs->removeElement($devoir)) {
            // set the owning side to null (unless already changed)
            if ($devoir->getMatiere() === $this) {
                $devoir->setMatiere(null);
            }
        }

        return $this;
    }

    public function getCoefficient()
    {
        return $this->coefficient;
    }

    public function setCoefficient($coefficient)
    {
        $this->coefficient = $coefficient;

        return $this;
    }

    public function getMoyenneM(): ?float
    {
        return $this->moyenneM;
    }

    public function setMoyenneM(?float $moyenneM): self
    {
        $this->moyenneM = $moyenneM;

        return $this;
    }



    /**
     * @return Collection|seance[]
     */
    public function getSeances(): Collection
    {
        return $this->seances;
    }

    public function addSeance(seance $seance): self
    {
        if (!$this->seances->contains($seance)) {
            $this->seances[] = $seance;
            $seance->setMatiere($this);
        }

        return $this;
    }

    public function removeSeance(seance $seance): self
    {
        if ($this->seances->removeElement($seance)) {
            // set the owning side to null (unless already changed)
            if ($seance->getMatiere() === $this) {
                $seance->setMatiere(null);
            }
        }

        return $this;
    }





    public function __toString()
    {
        return $this->getLibelle() ;
    }

    /**
     * @return Collection|Professor[]
     */
    public function getProfessors(): Collection
    {
        return $this->professors;
    }

    public function addProfessor(Professor $professor): self
    {
        if (!$this->professors->contains($professor)) {
            $this->professors[] = $professor;
            $professor->addMatiere($this);
        }

        return $this;
    }

    public function removeProfessor(Professor $professor): self
    {
        if ($this->professors->removeElement($professor)) {
            $professor->removeMatiere($this);
        }

        return $this;
    }

    /**
     * @return Collection|Niveau[]
     */
    public function getNiveaux(): Collection
    {
        return $this->niveaux;
    }

    public function addNiveau(Niveau $niveau): self
    {
        if (!$this->niveaux->contains($niveau)) {
            $this->niveaux[] = $niveau;
        }

        return $this;
    }

    public function removeNiveau(Niveau $niveau): self
    {
        $this->niveaux->removeElement($niveau);

        return $this;
    }


}
