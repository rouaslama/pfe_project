<?php

namespace App\Entity;

use App\Repository\TypePunitionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypePunitionRepository::class)
 */
class TypePunition
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity=Punition::class, mappedBy="typePunition")
     */
    private $punitions;

    public function __construct()
    {
        $this->punitions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection|Punition[]
     */
    public function getPunitions(): Collection
    {
        return $this->punitions;
    }

    public function addPunition(Punition $punition): self
    {
        if (!$this->punitions->contains($punition)) {
            $this->punitions[] = $punition;
            $punition->setTypePunition($this);
        }

        return $this;
    }

    public function removePunition(Punition $punition): self
    {
        if ($this->punitions->removeElement($punition)) {
            // set the owning side to null (unless already changed)
            if ($punition->getTypePunition() === $this) {
                $punition->setTypePunition(null);
            }
        }

        return $this;
    }
}
