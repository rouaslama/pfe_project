<?php

namespace App\Entity;

use App\Repository\PeriodeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=PeriodeRepository::class)
 */
class Periode
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Length (
     *     min = 1,
     *     max = 1,
     *      minMessage = "le num de periode doit faire {{ limit }} chiffre",
     *      maxMessage = "le num de periode doit faire {{ limit }} chiffre"
     * )
     */
    private $numPeriode;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateDebut;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateFin;

    /**
     * @ORM\ManyToOne(targetEntity=AnneeScolaire::class, inversedBy="periodes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $anneeScolaire;

    /**
     * @ORM\ManyToMany(targetEntity=Bulletin::class, inversedBy="periodes")
     */
    private $bulletins;

    /**
     * @ORM\Column(type="boolean")
     */
    private $cloturee;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $moyenneP;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity=Note::class, mappedBy="periode")
     */
    private $notes;

    public function __construct()
    {
        $this->bulletins = new ArrayCollection();
        $this->notes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(?\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(?\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getAnneeScolaire(): ?AnneeScolaire
    {
        return $this->anneeScolaire;
    }

    public function setAnneeScolaire(?AnneeScolaire $anneeScolaire): self
    {
        $this->anneeScolaire = $anneeScolaire;

        return $this;
    }

    /**
     * @return Collection|Bulletin[]
     */
    public function getBulletins(): Collection
    {
        return $this->bulletins;
    }

    public function addBulletin(Bulletin $bulletin): self
    {
        if (!$this->bulletins->contains($bulletin)) {
            $this->bulletins[] = $bulletin;
        }

        return $this;
    }

    public function removeBulletin(Bulletin $bulletin): self
    {
        $this->bulletins->removeElement($bulletin);

        return $this;
    }

    public function getCloturee(): ?bool
    {
        return $this->cloturee;
    }

    public function setCloturee(bool $cloturee): self
    {
        $this->cloturee = $cloturee;

        return $this;
    }

    public function getMoyenneP(): ?float
    {
        return $this->moyenneP;
    }

    public function setMoyenneP(?float $moyenneP): self
    {
        $this->moyenneP = $moyenneP;

        return $this;
    }


    public function getLibelle(): ?string
    {
        return $this->numPeriode.' ('.$this->getAnneeScolaire().')';
    }

    public function setLibelle(?string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }
    public function __toString()
    {
        return $this->numPeriode.' ('.$this->getAnneeScolaire().')' ;
    }

    /**
     * @return Collection|Note[]
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    public function addNote(Note $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes[] = $note;
            $note->setPeriode($this);
        }

        return $this;
    }

    public function removeNote(Note $note): self
    {
        if ($this->notes->removeElement($note)) {
            // set the owning side to null (unless already changed)
            if ($note->getPeriode() === $this) {
                $note->setPeriode(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumPeriode()
    {
        return $this->numPeriode;
    }

    /**
     * @param mixed $numPeriode
     * @return Periode
     */
    public function setNumPeriode($numPeriode)
    {
        $this->numPeriode = $numPeriode;
        return $this;
    }
}
