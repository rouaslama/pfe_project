<?php

namespace App\Entity;

use App\Repository\DevoirRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use http\Exception\RuntimeException;

/**
 * @ORM\Entity(repositoryClass=DevoirRepository::class)
 */
class Devoir
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="date")
     */
    private $date;


    /**
     * @ORM\ManyToOne(targetEntity=Matiere::class, inversedBy="devoirs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $matiere;


    /**
     * @ORM\Column(type="time")
     */
    private $hDebut;

    /**
     * @ORM\Column(type="time")
     */
    private $hFin;

    /**
     * @ORM\OneToMany(targetEntity=Note::class, mappedBy="devoir")
     */
    private $note;

    /**
     * @ORM\ManyToOne(targetEntity=Type::class, inversedBy="devoirs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    /**
     * @ORM\ManyToMany(targetEntity=Groupe::class, inversedBy="devoirs")
     */
    private $groupes;




    public function __construct()
    {
        $this->note = new ArrayCollection();
        $this->groupes = new ArrayCollection();
    }



    //*************************************************************************************************

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getHDebut()
    {
        return $this->hDebut;
    }

    /**
     * @param mixed $hDebut
     * @return Devoir
     */
    public function setHDebut($hDebut)
    {
        $this->hDebut = $hDebut;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHFin()
    {
        return $this->hFin;
    }

    /**
     * @param mixed $hFin
     * @return Devoir
     */
    public function setHFin($hFin)
    {
        $this->hFin = $hFin;
        return $this;
    }


    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }



    public function getMatiere(): ?Matiere
    {
        return $this->matiere;
    }

    public function setMatiere(?Matiere $matiere): self
    {
        $this->matiere = $matiere;

        return $this;
    }





    /**
     * @return Collection|Note[]
     */
    public function getNote(): Collection
    {
        return $this->note;
    }

    public function addNote(Note $note): self
    {
        if (!$this->note->contains($note)) {
            $this->note[] = $note;
            $note->setDevoir($this);
        }

        return $this;
    }

    public function removeNote(Note $note): self
    {
        if ($this->note->removeElement($note)) {
            // set the owning side to null (unless already changed)
            if ($note->getDevoir() === $this) {
                $note->setDevoir(null);
            }
        }

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Groupe[]
     */
    public function getGroupes(): Collection
    {
        return $this->groupes;
    }

    public function addGroupe(Groupe $groupe): self
    {
        if (!$this->groupes->contains($groupe)) {
            $this->groupes[] = $groupe;
            $groupe->addDevoir($this);
        }

        return $this;
    }

    public function removeGroupe(Groupe $groupe): self
    {
        if ($this->groupes->removeElement($groupe)) {
            $groupe->removeDevoir($this);
        }

        return $this;
    }
    public function __toString()
    {
        return '#'.$this->id.' '.$this->getMatiere() ;
    }


}
