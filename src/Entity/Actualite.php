<?php

namespace App\Entity;

use App\Repository\ActualiteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Exception;


/**
 * @ORM\Entity(repositoryClass=ActualiteRepository::class)
 * @Vich\Uploadable()
 */
class Actualite
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fichier;

    /**
     * @Vich\UploadableField(mapping="documents", fileNameProperty="fichier")
     * @var File
     */
    private $documentFile;


    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @var DateTime
     */
    private $dateCreation;

    /**
     * @ORM\ManyToMany(targetEntity=Groupe::class, inversedBy="actualites")
     * @ORM\JoinColumn (nullable=true)
     */
    private $groupes;

    /**
     * @ORM\ManyToMany(targetEntity=Professor::class,inversedBy="actualites")
     */
    private $professors;

    /**
     * @ORM\ManyToMany(targetEntity=HygieneOfficer::class, mappedBy="actualites")
     */
    private $hygieneOfficers;
    //*******************************************************************************************************************

    public function __construct(){
        $this->dateCreation = new DateTime();
        $this->groupes = new ArrayCollection();
        $this->professors = new ArrayCollection();
        $this->hygieneOfficers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(?string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getFichier(): ?string
    {
        return $this->fichier;
    }

    public function setFichier(?string $fichier): self
    {
        $this->fichier = $fichier;

        return $this;
    }


    /**
     * @return File
     */
    public function getDocumentFile(): ?File
    {
        return $this->documentFile;
    }

    /**
     * @param File $documentFile
     * @return void
     * @throws Exception
     */
    public function setDocumentFile(File $documentFile): void
    {
        $this->documentFile = $documentFile;

        if ($documentFile) {
            $this->dateCreation = new DateTime('now');
        }

    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(\DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * @return Collection|Groupe[]
     */
    public function getGroupes(): Collection
    {
        return $this->groupes;
    }

    public function addGroupe(Groupe $groupe): self
    {
        if (!$this->groupes->contains($groupe)) {
            $this->groupes[] = $groupe;
        }

        return $this;
    }

    public function removeGroupe(Groupe $groupe): self
    {
        $this->groupes->removeElement($groupe);

        return $this;
    }

    /**
     * @return Collection|Professor[]
     */
    public function getProfessors(): Collection
    {
        return $this->professors;
    }

    public function addProfessor(Professor $professor): self
    {
        if (!$this->professors->contains($professor)) {
            $this->professors[] = $professor;
            $professor->addActualite($this);
        }

        return $this;
    }

    public function removeProfessor(Professor $professor): self
    {
        if ($this->professors->removeElement($professor)) {
            $professor->removeActualite($this);
        }

        return $this;
    }

    /**
     * @return Collection|HygieneOfficer[]
     */
    public function getHygieneOfficers(): Collection
    {
        return $this->hygieneOfficers;
    }

    public function addHygieneOfficer(HygieneOfficer $hygieneOfficer): self
    {
        if (!$this->hygieneOfficers->contains($hygieneOfficer)) {
            $this->hygieneOfficers[] = $hygieneOfficer;
            $hygieneOfficer->addActualite($this);
        }

        return $this;
    }

    public function removeHygieneOfficer(HygieneOfficer $hygieneOfficer): self
    {
        if ($this->hygieneOfficers->removeElement($hygieneOfficer)) {
            $hygieneOfficer->removeActualite($this);
        }

        return $this;
    }
}
