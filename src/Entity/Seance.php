<?php

namespace App\Entity;

use App\Repository\SeanceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=SeanceRepository::class)
 */
class Seance
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $jour;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Length(
     *     min = 1,
     *     max = 1,
     *      minMessage = "le num de seance doit se composer d'un seul chiffre",
     *      maxMessage = "le num de telephone doit se composer d'un seul chiffre"
     * )
     */
    private $numSeance;

    /**
     * @ORM\ManyToOne(targetEntity=Professor::class, inversedBy="seances")
     * @ORM\JoinColumn(nullable=false)
     */
    private $professor;

    /**
     * @ORM\ManyToOne(targetEntity=Groupe::class, inversedBy="seance")
     * @ORM\JoinColumn(nullable=false)
     */
    private $groupe;

    /**
     * @ORM\ManyToOne(targetEntity=Matiere::class, inversedBy="seances")
     * @ORM\JoinColumn(nullable=false)
     */
    private $matiere;

    /**
     * @ORM\ManyToOne(targetEntity=Salle::class, inversedBy="seances")
     * @ORM\JoinColumn(nullable=false)
     */
    private $salle;


    /**
     * @ORM\OneToMany(targetEntity=Absence::class, mappedBy="seance")
     */
    private $absences;



    public function __construct()
    {
        $this->absences = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJour(): ?string
    {
        return $this->jour;
    }

    public function setJour(string $jour): self
    {
        $this->jour = $jour;

        return $this;
    }


    public function getProfessor(): ?Professor
    {
        return $this->professor;
    }

    public function setProfessor(?Professor $professor): self
    {
        $this->professor = $professor;

        return $this;
    }

    public function getGroupe(): ?Groupe
    {
        return $this->groupe;
    }

    public function setGroupe(?Groupe $groupe): self
    {
        $this->groupe = $groupe;

        return $this;
    }

    public function getMatiere(): ?Matiere
    {
        return $this->matiere;
    }

    public function setMatiere(?Matiere $matiere): self
    {
        $this->matiere = $matiere;

        return $this;
    }

    public function getSalle(): ?Salle
    {
        return $this->salle;
    }

    public function setSalle(?Salle $salle): self
    {
        $this->salle = $salle;

        return $this;
    }


    /**
     * @return Collection|Absence[]
     */
    public function getAbsences(): Collection
    {
        return $this->absences;
    }

    public function addAbsence(Absence $absence): self
    {
        if (!$this->absences->contains($absence)) {
            $this->absences[] = $absence;
            $absence->setSeance($this);
        }

        return $this;
    }

    public function removeAbsence(Absence $absence): self
    {
        if ($this->absences->removeElement($absence)) {
            // set the owning side to null (unless already changed)
            if ($absence->getSeance() === $this) {
                $absence->setSeance(null);
            }
        }

        return $this;
    }

    public function getNumSeance(): ?int
    {
        return $this->numSeance;
    }

    public function setNumSeance(int $numSeance): self
    {
        $this->numSeance = $numSeance;

        return $this;
    }
    public function __toString()
    {
        return '#'.$this->getId().' seance n° :'.$this->getNumSeance();
    }

}
