<?php

namespace App\Entity;

use App\Repository\AnneeScolaireRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AnneeScolaireRepository::class)
 */
class AnneeScolaire
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $debutAnnee;

    /**
     * @ORM\Column(type="integer")
     */
    private $finAnnee;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity=Niveau::class, mappedBy="anneeScolaire")
     */
    private $niveaux;

    /**
     * @ORM\OneToMany(targetEntity=Periode::class, mappedBy="anneeScolaire")
     */
    private $periodes;

    /**
     * @ORM\Column(type="boolean")
     */
    private $cloturee;

    public function __construct()
    {
        $this->niveaux = new ArrayCollection();
        $this->periodes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDebutAnnee(): ?int
    {
        return $this->debutAnnee;
    }

    public function setDebutAnnee(int $debutAnnee): self
    {
        $this->debutAnnee = $debutAnnee;

        return $this;
    }

    public function getFinAnnee(): ?int
    {
        return $this->finAnnee;
    }

    public function setFinAnnee(int $finAnnee): self
    {
        $this->finAnnee = $finAnnee;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection|Niveau[]
     */
    public function getNiveaux(): Collection
    {
        return $this->niveaux;
    }

    public function addNiveau(Niveau $niveau): self
    {
        if (!$this->niveaux->contains($niveau)) {
            $this->niveaux[] = $niveau;
            $niveau->setAnneeScolaire($this);
        }

        return $this;
    }

    public function removeNiveau(Niveau $niveau): self
    {
        if ($this->niveaux->removeElement($niveau)) {
            // set the owning side to null (unless already changed)
            if ($niveau->getAnneeScolaire() === $this) {
                $niveau->setAnneeScolaire(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Periode[]
     */
    public function getPeriodes(): Collection
    {
        return $this->periodes;
    }

    public function addPeriode(Periode $periode): self
    {
        if (!$this->periodes->contains($periode)) {
            $this->periodes[] = $periode;
            $periode->setAnneeScolaire($this);
        }

        return $this;
    }

    public function removePeriode(Periode $periode): self
    {
        if ($this->periodes->removeElement($periode)) {
            // set the owning side to null (unless already changed)
            if ($periode->getAnneeScolaire() === $this) {
                $periode->setAnneeScolaire(null);
            }
        }

        return $this;
    }

    public function getCloturee(): ?bool
    {
        return $this->cloturee;
    }

    public function setCloturee(bool $cloturee): self
    {
        $this->cloturee = $cloturee;

        return $this;
    }

    public function __toString()
    {
        return $this->getLibelle();
    }
}
