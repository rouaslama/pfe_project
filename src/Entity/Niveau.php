<?php

namespace App\Entity;

use App\Repository\NiveauRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NiveauRepository::class)
 */
class Niveau
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\ManyToOne(targetEntity=AnneeScolaire::class, inversedBy="niveaux")
     * @ORM\JoinColumn(nullable=false)
     */
    private $anneeScolaire;


    /**
     * @ORM\OneToMany(targetEntity=Groupe::class, mappedBy="niveau")
     * @ORM\JoinColumn(nullable=false)

     */
    private $groupes;



    /**
     * @ORM\ManyToMany(targetEntity=Frais::class, mappedBy="niveaux")
     */
    private $listFrais;

    /**
     * @ORM\ManyToMany(targetEntity=Matiere::class, mappedBy="niveaux")
     */
    private $matieres;

    public function __construct()
    {
        $this->groupes = new ArrayCollection();
        $this->listFrais = new ArrayCollection();
        $this->matieres = new ArrayCollection();
    }

    //********************************************************************************************************

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getAnneeScolaire(): ?AnneeScolaire
    {
        return $this->anneeScolaire;
    }

    public function setAnneeScolaire(?AnneeScolaire $anneeScolaire): self
    {
        $this->anneeScolaire = $anneeScolaire;

        return $this;
    }

    /**
     * @return Collection|Groupe[]
     */
    public function getGroupes(): Collection
    {
        return $this->groupes;
    }

    public function addGroupe(Groupe $groupe): self
    {
        if (!$this->groupes->contains($groupe)) {
            $this->groupes[] = $groupe;
            $groupe->setNiveau($this);
        }

        return $this;
    }

    public function removeGroupe(Groupe $groupe): self
    {
        if ($this->groupes->removeElement($groupe)) {
            // set the owning side to null (unless already changed)
            if ($groupe->getNiveau() === $this) {
                $groupe->setNiveau(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|Frais[]
     */
    public function getListFrais(): Collection
    {
        return $this->listFrais;
    }

    public function addListFrai(Frais $listFrai): self
    {
        if (!$this->listFrais->contains($listFrai)) {
            $this->listFrais[] = $listFrai;
        }

        return $this;
    }

    public function removeListFrai(Frais $listFrai): self
    {
        $this->listFrais->removeElement($listFrai);

        return $this;
    }
    public function __toString()
    {
        return $this->getLibelle().'('.$this->getAnneeScolaire().')';
    }

    /**
     * @return Collection|Matiere[]
     */
    public function getMatieres(): Collection
    {
        return $this->matieres;
    }

    public function addMatiere(Matiere $matiere): self
    {
        if (!$this->matieres->contains($matiere)) {
            $this->matieres[] = $matiere;
            $matiere->addNiveau($this);
        }

        return $this;
    }

    public function removeMatiere(Matiere $matiere): self
    {
        if ($this->matieres->removeElement($matiere)) {
            $matiere->removeNiveau($this);
        }

        return $this;
    }
}
