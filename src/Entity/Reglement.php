<?php

namespace App\Entity;

use App\Repository\ReglementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReglementRepository::class)
 */
class Reglement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $modeRegl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $statusFrais;



    /**
     * @ORM\Column(type="datetime")
     */
    private $dateReglement;

    /**
     * @ORM\ManyToOne(targetEntity=Student::class, inversedBy="reglements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $student;

    /**
     * @ORM\ManyToOne(targetEntity=Frais::class, inversedBy="reglements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $frais;

    /**
     * @ORM\ManyToOne(targetEntity=Tranche::class, inversedBy="reglement")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tranche;


    public function __construct()
    {
        $this->dateReglement = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModeRegl(): ?string
    {
        return $this->modeRegl;
    }

    public function setModeRegl(string $modeRegl): self
    {
        $this->modeRegl = $modeRegl;

        return $this;
    }



    public function getDateReglement(): ?\DateTimeInterface
    {
        return $this->dateReglement;
    }

    public function setDateReglement(\DateTimeInterface $dateReglement): self
    {
        $this->dateReglement = $dateReglement;

        return $this;
    }

    public function getStudent(): ?Student
    {
        return $this->student;
    }

    public function setStudent(?Student $student): self
    {
        $this->student = $student;

        return $this;
    }

    public function getFrais(): ?Frais
    {
        return $this->frais;
    }

    public function setFrais(?Frais $frais): self
    {
        $this->frais = $frais;

        return $this;
    }


    public function getTranche(): ?Tranche
    {
        return $this->tranche;
    }

    public function setTranche(?Tranche $tranche): self
    {
        $this->tranche = $tranche;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatusFrais()
    {
        return $this->statusFrais;
    }

    /**
     * @param mixed $statusFrais
     * @return Reglement
     */
    public function setStatusFrais($statusFrais)
    {
        $this->statusFrais = $statusFrais;
        return $this;
    }





}
