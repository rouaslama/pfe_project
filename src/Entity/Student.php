<?php

namespace App\Entity;

use App\Repository\StudentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=StudentRepository::class)
 */
class Student extends User implements UserInterface
{

    /**
     * @ORM\ManyToOne(targetEntity=Tutor::class, inversedBy="students")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tutor;



    /**
     * @ORM\OneToMany(targetEntity=Note::class, mappedBy="student")
     */
    private $notes;


    /**
     * @ORM\ManyToMany(targetEntity=Absence::class, mappedBy="students")
     */
    private $absences;

    /**
     * @ORM\ManyToMany(targetEntity=Punition::class, mappedBy="students")
     */
    private $punitions;

    /**
     * @ORM\ManyToOne(targetEntity=Groupe::class, inversedBy="students")
     * @ORM\JoinColumn (nullable=true)
     */
    private $groupe;

    /**
     * @ORM\OneToMany(targetEntity=Reglement::class, mappedBy="student")
     */
    private $reglements;


    public function __construct()
    {
        parent::__construct();
        $this->notes = new ArrayCollection();
        $this->absences = new ArrayCollection();
        $this->punitions = new ArrayCollection();
        $this->reglements = new ArrayCollection();

    }


    public function getTutor(): ?Tutor
    {
        return $this->tutor;
    }

    public function setTutor(?Tutor $tutor): self
    {
        $this->tutor = $tutor;

        return $this;
    }
    /**
     * @return Tutor|null
     */
    public function getTutorEmail(): ?string
    {
        if (isset($this)) {
            return $this->tutor->getEmail();
        }
    }


    /**
     * @return Collection|Note[]
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    public function addNote(Note $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes[] = $note;
            $note->setStudent($this);
        }

        return $this;
    }

    public function removeNote(Note $note): self
    {
        if ($this->notes->removeElement($note)) {
            // set the owning side to null (unless already changed)
            if ($note->getStudent() === $this) {
                $note->setStudent(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|Absence[]
     */
    public function getAbsences(): Collection
    {
        return $this->absences;
    }

    public function addAbsence(Absence $absence): self
    {
        if (!$this->absences->contains($absence)) {
            $this->absences[] = $absence;
            $absence->addStudent($this);
        }

        return $this;
    }

    public function removeAbsence(Absence $absence): self
    {
        if ($this->absences->removeElement($absence)) {
            $absence->removeStudent($this);
        }

        return $this;
    }

    /**
     * @return Collection|Punition[]
     */
    public function getPunitions(): Collection
    {
        return $this->punitions;
    }

    public function addPunition(Punition $punition): self
    {
        if (!$this->punitions->contains($punition)) {
            $this->punitions[] = $punition;
            $punition->addStudent($this);
        }

        return $this;
    }

    public function removePunition(Punition $punition): self
    {
        if ($this->punitions->removeElement($punition)) {
            $punition->removeStudent($this);
        }

        return $this;
    }


    public function getGroupe(): ?Groupe
    {
        return $this->groupe;
    }

    public function setGroupe(?Groupe $groupe): self
    {
        $this->groupe = $groupe;

        return $this;
    }


    public function __toString()
    {
//        $niv = $this->getGroupe()->getNiveau()->getLibelle();
//        //dd($niv);
//        $gr = $this->getGroupe()->getLibelle();
//        //dd($gr);
//        $annee = $this->getGroupe()->getNiveau()->getAnneeScolaire();
//        $nivGrAn = '('.$niv.' '.$gr.' '.$annee.')';
        //dd($nivGrAn);
        return '#'.$this->getId().' '.$this->getFirstName().' '.$this->getLastName();//.$nivGrAn;
    }


    /**
     * @return Collection|Reglement[]
     */

   public function getReglements(): Collection
    {
        return $this->reglements;
    }

    public function addReglement(Reglement $reglement): self
    {
        if (!$this->reglements->contains($reglement)) {
            $this->reglements[] = $reglement;
            $reglement->setStudent($this);
        }

        return $this;
    }

    public function removeReglement(Reglement $reglement): self
    {
        if ($this->reglements->removeElement($reglement)) {
            // set the owning side to null (unless already changed)
            if ($reglement->getStudent() === $this) {
                $reglement->setStudent(null);
            }
        }

        return $this;
    }




}