<?php

namespace App\Entity;

use App\Repository\FraisRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FraisRepository::class)
 */
class Frais
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")_
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\ManyToMany(targetEntity=Niveau::class, inversedBy="listFrais")
     */
    private $niveaux;

    /**
     * @ORM\OneToMany(targetEntity=Reglement::class, mappedBy="frais")
     * @ORM\JoinColumn (nullable=true)
     */
    private $reglements;

    /**
     * @ORM\OneToMany(targetEntity=Tranche::class, mappedBy="frais")
     * @ORM\JoinColumn (nullable=true)
     */
    private $tranches;


    public function __construct()
    {
        $this->niveaux = new ArrayCollection();
        $this->reglements = new ArrayCollection();
        $this->tranches = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param mixed $libelle
     * @return Frais
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
        return $this;
    }


    /**
     * @return Collection|Niveau[]
     */
    public function getNiveaux(): Collection
    {
        return $this->niveaux;
    }

    public function addNiveau(Niveau $niveau): self
    {
        if (!$this->niveaux->contains($niveau)) {
            $this->niveaux[] = $niveau;
            $niveau->addListFrai($this);
        }

        return $this;
    }

    public function removeNiveau(Niveau $niveau): self
    {
        if ($this->niveaux->removeElement($niveau)) {
            $niveau->removeListFrai($this);
        }

        return $this;
    }

    /**
     * @return Collection|Reglement[]
     */

    public function getReglements(): Collection
    {
        return $this->reglements;
    }

    public function addReglement(Reglement $reglement): self
    {
        if (!$this->reglements->contains($reglement)) {
            $this->reglements[] = $reglement;
            $reglement->setFrais($this);
        }

        return $this;
    }

    public function removeReglement(Reglement $reglement): self
    {
        if ($this->reglements->removeElement($reglement)) {
            // set the owning side to null (unless already changed)
            if ($reglement->getFrais() === $this) {
                $reglement->setFrais(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|Tranche[]
     */

    public function getTranches(): Collection
    {
        return $this->tranches;
    }

    public function addTranch(Tranche $tranch): self
    {
        if (!$this->tranches->contains($tranch)) {
            $this->tranches[] = $tranch;
            $tranch->setFrais($this);
        }

        return $this;
    }


    public function removeTranch(Tranche $tranch): self
    {
        if ($this->tranches->removeElement($tranch)) {
            // set the owning side to null (unless already changed)
            if ($tranch->getFrais() === $this) {
                $tranch->setFrais(null);
            }
        }

        return $this;
    }

    public function __toString()
    {

        $niveaux = $this->getNiveaux();

        $toutNivA = '';
        foreach ($niveaux as $niv) {
            $nivA = $niv->getLibelle() .' '. $niv->getAnneeScolaire()->getLibelle();
//            "{$str1}{$str2}{$str3}";  //fast concat
            $toutNivA .= '|' . $nivA;
        }
        return $this->getLibelle();//.'->('.$toutNivA.')';
    }


}
