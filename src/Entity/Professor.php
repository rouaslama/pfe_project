<?php

namespace App\Entity;

use App\Repository\ProfessorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ProfessorRepository::class)
 */
class Professor extends User implements UserInterface
{


    /**
     * @ORM\Column(type="integer", length=8, unique=true)
     * @Assert\Length(
     *     min = 8,
     *     max = 8,
     *      minMessage = "le num de CIN doit faire {{ limit }} chiffres",
     *      maxMessage = "le num de CIN doit faire {{ limit }} chiffres"
     * )
     */
    private $cin;




    /**
     * @ORM\OneToMany(targetEntity=Seance::class, mappedBy="professor")
     */
    private $seances;

    /**
     * @ORM\OneToMany(targetEntity=Absence::class, mappedBy="professor")
     */
    private $absences;

    /**
     * @ORM\ManyToMany(targetEntity=Matiere::class, inversedBy="professors")
     */
    private $matieres;



    /**
     * @ORM\ManyToMany(targetEntity=Actualite::class, mappedBy="professors")
     */
    private $actualites;


    public function __construct()
    {
        parent::__construct();
        $this->seances = new ArrayCollection();
        $this->absences = new ArrayCollection();
        $this->matieres = new ArrayCollection();
        $this->actualites = new ArrayCollection();
    }



    public function getCin(): ?int
    {
        return $this->cin;
    }

    public function setCin(int $cin): self
    {
        $this->cin = $cin;

        return $this;
    }



    /**
     * @return Collection|Seance[]
     */
    public function getSeances(): Collection
    {
        return $this->seances;
    }

    public function addSeance(Seance $seance): self
    {
        if (!$this->seances->contains($seance)) {
            $this->seances[] = $seance;
            $seance->setProfessor($this);
        }

        return $this;
    }

    public function removeSeance(Seance $seance): self
    {
        if ($this->seances->removeElement($seance)) {
            // set the owning side to null (unless already changed)
            if ($seance->getProfessor() === $this) {
                $seance->setProfessor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Absence[]
     */
    public function getAbsences(): Collection
    {
        return $this->absences;
    }

    public function addAbsence(Absence $absence): self
    {
        if (!$this->absences->contains($absence)) {
            $this->absences[] = $absence;
            $absence->setProfessor($this);
        }

        return $this;
    }

    public function removeAbsence(Absence $absence): self
    {
        if ($this->absences->removeElement($absence)) {
            // set the owning side to null (unless already changed)
            if ($absence->getProfessor() === $this) {
                $absence->setProfessor(null);
            }
        }

        return $this;
    }


    public function __toString()
    {
        return '#'.$this->getId().' '.$this->getFirstName().' '.$this->getLastName();
    }

    /**
     * @return Collection|Matiere[]
     */
    public function getMatieres(): Collection
    {
        return $this->matieres;
    }

    public function addMatiere(Matiere $matiere): self
    {
        if (!$this->matieres->contains($matiere)) {
            $this->matieres[] = $matiere;
        }

        return $this;
    }

    public function removeMatiere(Matiere $matiere): self
    {
        $this->matieres->removeElement($matiere);

        return $this;
    }



    /**
     * @return Collection|Actualite[]
     */
    public function getActualites(): Collection
    {
        return $this->actualites;
    }

    public function addActualite(Actualite $actualite): self
    {
        if (!$this->actualites->contains($actualite)) {
            $this->actualites[] = $actualite;
        }

        return $this;
    }

    public function removeActualite(Actualite $actualite): self
    {
        $this->actualites->removeElement($actualite);

        return $this;
    }

}
