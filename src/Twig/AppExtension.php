<?php

namespace App\Twig;

use App\Entity\Absence;
use App\Entity\Note;
use App\Entity\Professor;
use App\Entity\Student;
use App\Entity\User;
use App\Services\MessageService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('getProfessors', [$this, 'getProfessors']),
            new TwigFunction('getAbsences', [$this, 'getAbsences']),
            new TwigFunction('getNotes', [$this, 'getNotes']),

        ];
    }

    public function getProfessors(Student $student)
    {
        $grp=$student->getGroupe();
        $niveau = $grp->getNiveau();
        $matieres = $niveau->getMatieres();
        $enseigants = [];
        foreach ($matieres as $matiere){
            foreach ($matiere->getProfessors() as $enseigant)
                array_push($enseigants, $enseigant);
        }
        return $enseigants;
    }

    public function getAbsences(Student $student){
      $abs=$student->getAbsences();
      $absences = [];
        foreach ($abs as $ab) {
            $id=$ab->getId();
            $absence = $this->em->getRepository(Absence::class)->findBy(['id' => $id]);
            array_push($absences, $absence);
        }
        return $abs;
    }

    public function getNotes(Student $student){
          $notes=$student->getNotes();
          $tabNotes = [];
          foreach ($notes as $value) {
              $note = $this->em->getRepository(Note::class)->findBy(['student' => $value]);
              array_push($tabNotes, $note);
          }
        return $notes;
    }



}