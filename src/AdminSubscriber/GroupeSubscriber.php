<?php


namespace App\AdminSubscriber;


use App\Entity\Groupe;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class GroupeSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    private $session;

    /**
     * SeanceSubscriber constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager,  SessionInterface $session){
        $this->entityManager = $entityManager;
        $this->session = $session;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            AfterEntityPersistedEvent::class => ['newGroupe'],
        ];
    }


    /**
     * @param AfterEntityPersistedEvent $event
     */

    public function newGroupe(AfterEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if ($entity instanceof Groupe) {
            $lib = $entity->getLibelle();
            $niv = $entity->getNiveau();

            $groupes = $this->entityManager->getRepository(\App\Entity\Groupe::class)->findBy([
                "libelle" => $lib,
                "niveau" => $niv,

            ]);
//            dd($seances);
            if (count($groupes) > 1) {
                $this->entityManager->remove($entity);
                $this->entityManager->flush();
                $this->session->getFlashBag()->add('error', 'Désolé ce groupe existe déja !!');
            }

        }

    }
}