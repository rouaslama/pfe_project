<?php


namespace App\AdminSubscriber;
use App\Entity\Matiere;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class MatiereSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    private $session;

    /**
     * SeanceSubscriber constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager,  SessionInterface $session){
        $this->entityManager = $entityManager;
        $this->session = $session;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            AfterEntityPersistedEvent::class => ['newMatiere'],
        ];
    }


    /**
     * @param AfterEntityPersistedEvent $event
     */

    public function newMatiere(AfterEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if ($entity instanceof Matiere) {
            $lib = $entity->getLibelle();

            $groupes = $this->entityManager->getRepository(\App\Entity\Matiere::class)->findBy([
                "libelle" => $lib,

            ]);
//            dd($seances);
            if (count($groupes) > 1) {
                $this->entityManager->remove($entity);
                $this->entityManager->flush();
                $this->session->getFlashBag()->add('error', 'désolé cette matière existe déja !!');
            }

        }

    }
}