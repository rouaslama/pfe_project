<?php


namespace App\AdminSubscriber;


use App\Entity\Seance;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SeanceSubscriber implements EventSubscriberInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    private $session;

    /**
     * SeanceSubscriber constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager,  SessionInterface $session){
        $this->entityManager = $entityManager;
        $this->session = $session;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            AfterEntityPersistedEvent::class => ['newSeance'],
        ];
    }


    /**
     * @param AfterEntityPersistedEvent $event
     */

    public function newSeance(AfterEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if ($entity instanceof Seance) {
            $jour = $entity->getJour();
            $numSeance = $entity->getNumSeance();
            $groupe = $entity->getGroupe();
            $matiere = $entity->getMatiere(); // la matière choisie dans la séance
            $professor = $entity->getProfessor();
            $salle = $entity->getSalle();

            $matieres = $entity->getProfessor()->getMatieres(); //les matieres de l'enseignant choisi
            foreach ($matieres as $mat) {
                if ($matiere != $mat) {
                    $this->entityManager->remove($entity);
                    $this->entityManager->flush();
                    $this->session->getFlashBag()->add('error', "Cet enseignant n'enseigne pas la matière choisie !!" );
                }
            }
            //test toute la seance de ses composants existe déja
            $snces = $this->entityManager->getRepository(\App\Entity\Seance::class)->findBy([
                "jour" => $jour,
                "numSeance" => $numSeance,
                "groupe" => $groupe,
                "matiere" => $matiere,
                "professor" => $professor,
                "salle" => $salle,
            ]);
//            dd($seances);
            if (count($snces) > 1) {
//                dd('ok');
                $this->entityManager->remove($entity);
                $this->entityManager->flush();
                $this->session->getFlashBag()->add('error', 'désolé cette séance existe déja');
            }



            // test salle
            $seancesSalle = $this->entityManager->getRepository(\App\Entity\Seance::class)->findBy([
                "jour" => $jour,
                "numSeance" => $numSeance,
                "salle" => $salle,
            ]);
//            dd($seances);
            if (count($seancesSalle) > 1) {
                $this->entityManager->remove($entity);
                $this->entityManager->flush();
                $this->session->getFlashBag()->add('error', 'la salle choisie est occupée !!');
            }
            //test enseignant ne peut pas enseigner 2 seances en m temps
            $seances = $this->entityManager->getRepository(\App\Entity\Seance::class)->findBy([
                "jour" => $jour,
                "numSeance" => $numSeance,
                "professor" => $professor,
            ]);
//            dd($seances);
            if (count($seances) > 1) {
                $this->entityManager->remove($entity);
                $this->entityManager->flush();
                $this->session->getFlashBag()->add('error', "l'enseignant choisi a déja une autre séance !! ");
            }

        }


    }

}








