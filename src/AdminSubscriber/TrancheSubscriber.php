<?php


namespace App\AdminSubscriber;
use App\Entity\Tranche;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class TrancheSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    private $session;

    /**
     * SeanceSubscriber constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager,  SessionInterface $session){
        $this->entityManager = $entityManager;
        $this->session = $session;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            AfterEntityPersistedEvent::class => ['newTranche'],
        ];
    }


    /**
     * @param AfterEntityPersistedEvent $event
     */

    public function newTranche(AfterEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if ($entity instanceof Tranche) {
            $frais = $entity->getFrais();
            $numtr = $entity->getNumTranche();
            $montant = $entity->getMontant();


            $salles = $this->entityManager->getRepository(\App\Entity\Tranche::class)->findBy([
                "frais" => $frais,
                "numTranche" => $numtr,
                "montant" => $montant,

            ]);
//            dd($seances);
            if (count($salles) > 1) {
                $this->entityManager->remove($entity);
                $this->entityManager->flush();
                $this->session->getFlashBag()->add('error', 'désolé cette Tranche existe déja !!');
            }

        }

    }
}