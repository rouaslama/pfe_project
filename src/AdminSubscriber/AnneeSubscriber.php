<?php


namespace App\AdminSubscriber;


use App\Entity\AnneeScolaire;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class AnneeSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    private $session;

    /**
     * AnneeSubscriber constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session)
    {
        $this->entityManager = $entityManager;
        $this->session = $session;

    }


    public static function getSubscribedEvents(): array
    {
        return [
            BeforeEntityPersistedEvent::class => ['AjouterAnnee'],
            AfterEntityPersistedEvent::class => ['addAnnee'],
        ];
    }
    /**
     * @param BeforeEntityPersistedEvent $event
     */
    public function AjouterAnnee(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if ($entity instanceof AnneeScolaire){
            $debutAnnee = $entity->getDebutAnnee();
            $finAnnee = $entity->getFinAnnee();
            $lib = $debutAnnee.'-'.$finAnnee;
            $entity->setLibelle($lib);
            $entity->setDebutAnnee($debutAnnee);
            $entity->setFinAnnee($finAnnee);

            $lib = $entity->getLibelle();

        }
    }

    /**
     * @param AfterEntityPersistedEvent $event
     */

    public function addAnnee(AfterEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if ($entity instanceof AnneeScolaire) {
            $debutAnnee = $entity->getDebutAnnee();
            $finAnnee = $entity->getFinAnnee();
            $lib = $entity->getLibelle();

            $annees = $this->entityManager->getRepository(\App\Entity\AnneeScolaire::class)->findBy([
                "debutAnnee" => $debutAnnee,
                "finAnnee" => $finAnnee,
//                "libelle" => $lib,

            ]);
//            dd($annees);
            if (count($annees) > 1) {
                $this->entityManager->remove($entity);
                $this->entityManager->flush();
                $this->session->getFlashBag()->add('error', 'Désolé cette année scolaire existe déja !!');
            }
        }
    }




}