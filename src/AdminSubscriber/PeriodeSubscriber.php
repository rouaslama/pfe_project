<?php


namespace App\AdminSubscriber;

use App\Entity\AnneeScolaire;
use App\Entity\Periode;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class PeriodeSubscriber  implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    private $session;

    /**
     * SeanceSubscriber constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager,  SessionInterface $session){
        $this->entityManager = $entityManager;
        $this->session = $session;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            AfterEntityPersistedEvent::class => ['newPeriode'],
        ];
    }


    /**
     * @param AfterEntityPersistedEvent $event
     */

    public function newPeriode(AfterEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if ($entity instanceof Periode) {
            $numP = $entity->getNumPeriode();
            $annee = $entity->getAnneeScolaire();

            $periodes = $this->entityManager->getRepository(\App\Entity\Periode::class)->findBy([
                "numPeriode" => $numP,
                "anneeScolaire" => $annee,

            ]);
//            dd($seances);
            if (count($periodes) > 1) {
                $this->entityManager->remove($entity);
                $this->entityManager->flush();
                $this->session->getFlashBag()->add('error', 'désolé cette période existe déja !!');
            }

            $perds = $this->entityManager->getRepository(\App\Entity\AnneeScolaire::class)->findBy([
//                "anneeScolaire" => 1
                    "cloturee" => 1
            ]);
//            dd($seances);
            if (count($perds) > 0 ) {
                $this->entityManager->remove($entity);
                $this->entityManager->flush();
                $this->session->getFlashBag()->add('error', "Désolé l'année sélectionnée est cloturée !!");
            }



        }

    }
}