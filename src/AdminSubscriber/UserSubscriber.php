<?php


namespace App\AdminSubscriber;

use App\Entity\HygieneOfficer;
use App\Entity\Professor;
use App\Entity\Tutor;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityDeletedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityUpdatedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityDeletedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

use App\Entity\User;
use App\Entity\Student;
use App\Entity\Contact;

use App\Services\PasswordGenerator;
use App\Services\SwiftMailerService;

use Symfony\Component\EventDispatcher\GenericEvent;


class UserSubscriber implements EventSubscriberInterface
{
    const TEMPLATE_CONTACT = "email/contact.html.twig";

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PasswordGenerator
     */
    protected $passwordGenerator;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @var SwiftMailerService
     */
    private $mailerService;
    private $session;


    public function __construct(EntityManagerInterface $entityManager,PasswordGenerator $passwordGenerator, UserPasswordEncoderInterface $passwordEncoder, SwiftMailerService $mailerService, SessionInterface $session)
    {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->mailerService = $mailerService;
        $this->passwordGenerator = $passwordGenerator;
        $this->session = $session;

    }

    public static function getSubscribedEvents(): array
    {
        return [
            BeforeEntityPersistedEvent::class => ['addUser'],
            AfterEntityPersistedEvent::class => ['addStudent'],
            BeforeEntityDeletedEvent::class => ['deleteStudent'],
            AfterEntityUpdatedEvent::class => ['updateStudent'],
        ];
    }

    /**
     * @param BeforeEntityPersistedEvent $event
     */
    public function addUser(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if ($entity instanceof User) {

            // set roles
            $s = array('ROLE_STUDENT');
            $t = array('ROLE_TUTOR');
            $p = array('ROLE_PROFESSOR');
            $h = array('ROLE_HYGIENE_OFFICER');

            if ($entity instanceof Student){
                $entity->setRoles($s);
            }
            if ($entity instanceof Tutor){
                $entity->setRoles($t);
            }
            if ($entity instanceof Professor){
                $entity->setRoles($p);
            }
            if ($entity instanceof HygieneOfficer){
                $entity->setRoles($h);
            }
            // ./setRoles

            $firstName = $entity->getFirstName();
            $random = $this->randomNumbers();
            $username = $firstName.$random;
            $entity->setUserName($username);
            //generate random password
            $plainPwd = $this->passwordGenerator->generateRandomStrongPassword(8);


            if($entity instanceof Student){
                //******* Student mail *********

                $tutorMail = $entity->getTutorEmail();
                $entity->setEmail($tutorMail);
                //******* Student mail

            }

            //send mail to the user containing the plain password
            $contact = $this->createContact($entity, $plainPwd);
            try {
                $this->sendContact($contact);
            } catch (LoaderError | RuntimeError | SyntaxError $e) {
            }

            //encrypt the generated password ($plainPwd)
            $this->cryptPassword($entity, $plainPwd);
            //persisting in DB
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }
    }


    /**
     * @param AfterEntityPersistedEvent $event
     */
    public function addStudent(AfterEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if ($entity instanceof Student) {
//            dd('ok');
            //nbEleves =nbEleves + 1
            if ($entity->getGroupe() != null)
            {
//                dd($entity->getGroupe());
                $g =$entity->getGroupe();
                $nb = $g->getNbEleves();
//                dd($nb + 1);
//                $g->setNbEleves($nb + 1);
//                $this->entityManager->persist($g);
//                $this->entityManager->flush();

//                dd($g);
                $g->setNbEleves($nb + 1);
                $nbPlus = $g->getNbEleves();
                $nbMax = $g->getNbMax();
                if ($nb + 1 > $nbMax){
                    $g->removeStudent($entity);
                     $g->setNbEleves($nbPlus - 1);

                    $this->session->getFlashBag()->add('error', "désolé ce groupe est complé !! essayez
                    d'inscrire l'élève dans un autre groupe ");
                }
//                $g->setNbEleves($nb + 1);
//                $this->entityManager->flush();


//                $g->setNbEleves($nb + 1);
                $this->entityManager->flush();


            }
            // /. nbEleves
        }
    }

    /**
     * @param BeforeEntityDeletedEvent $event
     */
    public function deleteStudent(BeforeEntityDeletedEvent  $event)
    {
        $entity = $event->getEntityInstance();

        if ($entity instanceof Student) {
            if ($entity->getGroupe() != null) {
                $g = $entity->getGroupe();
                $nb = $g->getNbEleves();
                $g->setNbEleves($nb - 1);
                $g->removeStudent($entity);
            }
        }
    }

/**
     * @param AfterEntityUpdatedEvent $event
     */
    public function updateStudent(AfterEntityUpdatedEvent  $event)
    {
        $entity = $event->getEntityInstance();

        if ($entity instanceof Student) {
            //            dd('ok');
            //nbEleves =nbEleves + 1
            if ($entity->getGroupe() != null)
            {
//                dd($entity->getGroupe());
                $g =$entity->getGroupe();
                $nb = $g->getNbEleves();
//                dd($nb + 1);
//                $g->setNbEleves($nb + 1);
//                $this->entityManager->persist($g);
//                $this->entityManager->flush();

//                dd($g);
                $g->setNbEleves($nb + 1);
                $nbPlus = $g->getNbEleves();
                $nbMax = $g->getNbMax();
                if ($nb + 1 > $nbMax){
                    $g->removeStudent($entity);
                    $g->setNbEleves($nbPlus - 1);

                    $this->session->getFlashBag()->add('error', "désolé ce groupe est complé !! essayez
                    d'inscrire l'élève dans un autre groupe ");
                }
//                $g->setNbEleves($nb + 1);
//                $this->entityManager->flush();


//                $g->setNbEleves($nb + 1);
                $this->entityManager->flush();


            }
        }
    }


    /**
     * @param User $entity
     * @param string $plainPwd
     * @return Contact
     */
    public function createContact(User $entity,string $plainPwd): Contact {

        //get data
        $firstName=$entity->getFirstName();
        $lastName=$entity->getLastName();
        $userName = $entity->getUserName();

        $name = $firstName.' '.$lastName ;
        $email=$entity->getEmail();
        $message= sprintf("le login du compte:%s \n le mot de passe du compte:%s",$userName,$plainPwd) ;

        //create Contact entity
        $contact= new Contact;
        $contact->setName($name);
        $contact->setEmail($email);
        $contact->setDescription($message);
        return $contact;

    }
    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function sendContact(Contact $contact)
    {
        $TEMPLATE_CONTACT = "email/contact.html.twig";
        $parameters = [
            "email" => $contact->getEmail(),
            "name" => $contact->getName(),
            "description" => $contact->getDescription()
        ];

        $this->mailerService->send(
            "Pascal school, mot de passe de votre compte",
            ['pascal.Primary.School@gmail.com'],
            [$contact->getEmail()],
            $TEMPLATE_CONTACT,
            $parameters
        );
    }


    /**
     * @param User $entity
     * @param string $plainPwd
     */
    public function cryptPassword(User $entity,string $plainPwd): void
    {
        $entity->setPassword(
            $this->passwordEncoder->encodePassword(
                $entity,
                $plainPwd
            )
        );

    }

    public function randomNumbers(): string
    {
        //returns a string of 4 random numbers :
        //It generates a random 4 character string consisting of, by default, only 0-9,
        // but you can change the value of $a for other characters. The random string will be in variable $s .

        for ($s = '', $i = 0, $z = strlen($a = '0123456789')-1; $i != 4; $x = rand(0,$z), $s .= $a{$x}, $i++);
        return $s;
    }


}