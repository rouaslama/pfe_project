<?php


namespace App\AdminSubscriber;


use App\Entity\Salle;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SalleSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    private $session;

    /**
     * SeanceSubscriber constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager,  SessionInterface $session){
        $this->entityManager = $entityManager;
        $this->session = $session;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            AfterEntityPersistedEvent::class => ['newSalle'],
        ];
    }


    /**
     * @param AfterEntityPersistedEvent $event
     */

    public function newSalle(AfterEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if ($entity instanceof Salle) {
            $nomS = $entity->getNom();


            $salles = $this->entityManager->getRepository(\App\Entity\Salle::class)->findBy([
                "nom" => $nomS,

            ]);
//            dd($seances);
            if (count($salles) > 1) {
                $this->entityManager->remove($entity);
                $this->entityManager->flush();
                $this->session->getFlashBag()->add('error', 'désolé cette salle existe déja !!');
            }

        }

    }
}