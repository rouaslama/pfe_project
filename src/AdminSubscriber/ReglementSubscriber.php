<?php


namespace App\AdminSubscriber;

use App\Entity\Reglement;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


class ReglementSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    private $session;

    /**
     * SeanceSubscriber constructor.
     * @param EntityManagerInterface $entityManager
     * @param SessionInterface $session
     */
    public function __construct(EntityManagerInterface $entityManager,  SessionInterface $session){
        $this->entityManager = $entityManager;
        $this->session = $session;
    }



    public static function getSubscribedEvents(): array
    {
        return [
            BeforeEntityPersistedEvent::class => ['newReglement'],
            AfterEntityPersistedEvent::class => ['addReglement'],
        ];
    }


    /**
     * @param BeforeEntityPersistedEvent $event
     */

    public function newReglement(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if ($entity instanceof Reglement) {
            $entity->setModeRegl('présentiel');
        }
    }

    /**
     * @param AfterEntityPersistedEvent $event
     */
    public function addReglement(AfterEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if ($entity instanceof Reglement) {

            //test nivElev != nivFrais
            $student = $entity->getStudent();
            $nivElv =$student->getGroupe()->getNiveau();
//            dd($nivElv);
            $nivxFrais = $entity->getFrais()->getNiveaux();
//            dd($nivxFrais);
            $i = 0;
            foreach ($nivxFrais as $niv){
                if ($niv == $nivElv) { $i = 1;}
            }
            if ($i != 1){
                $this->entityManager->remove($entity);
                $this->entityManager->flush();
                $this->session->getFlashBag()->add('error', "Le niveau de frais choisi ne correspond pas au niveau de l'élève !");
            }

            //test frais de tranche doit correspondre au frais choisi
            $trChoisie = $entity->getTranche();
            $frTranche = $trChoisie->getFrais();
            $frChoisi  =$entity->getFrais();
            if ($frTranche != $frChoisi){
                $this->entityManager->remove($entity);
                $this->entityManager->flush();
                $this->session->getFlashBag()->add('error', "La tranche choisie ne correspond pas au frais sélectionné !");
            }


            // **************** status frais ************************************************

            $regls = $this->entityManager->getRepository(Reglement::class)->findBy([
                "frais" => $frChoisi,
                "student" => $student,
            ]);
//            dd(count($regls));

            $tranchesF = $frChoisi->getTranches();
            $nbF = count($tranchesF);
//            dd($nbF);
            if (count($regls) < $nbF) {
                //status frais = non payé
                foreach ($regls as $regl) {
                    //   dd($regl);
                    $regl->setStatusFrais('non payé');
                    $this->entityManager->persist($entity);
                    $this->entityManager->flush();
                }

                if (count($regls) == 1) {
                    foreach ($regls as $reg) {
                        $numTr = $reg->getTranche()->getNumTranche();
                        if ($numTr != 1){
                            $this->entityManager->remove($entity);
                            $this->entityManager->flush();
                            $this->session->getFlashBag()->add('error', "le num de la première tranche payée doit être 1 !");
                        }
                    }
                }
                if (count($regls) > 1){
//                    dd($regls);

                    //************* reglement de tranche déja fait***********************
                    $reglements = $this->entityManager->getRepository(Reglement::class)->findBy([
                        "frais" => $frChoisi,
                        "student" => $student,
                        "tranche" => $trChoisie,
                    ]);
//            dd($reglements);

                    if (count($reglements) > 1){
//                    dd($reglements[1]);
                        $this->entityManager->remove($reglements[1]);
                        $this->entityManager->flush();
                        $this->session->getFlashBag()->add('error', "Le paiement de cette tranche est déja fait !");
                    }
                    //  /. reglement de tranche déja fait***********************

                    for ($i = 0; $i < count($regls)-1; $i++){
                        $numTrprec  = $regls[$i]->getTranche()->getNumTranche();
//                        dd($numTrprec);
                        $numTrSuiv = $regls[$i+1]->getTranche()->getNumTranche();
//                        dd($numTrSuiv);
                        if (isset($numTrSuiv)){
                            if ($numTrSuiv != $numTrprec + 1){
//                                dd($regls);
//                                dd($regls[$i + 1]);
                                $this->entityManager->remove($regls[$i + 1]);
                                $this->entityManager->flush();
                                $this->session->getFlashBag()->add('error', "Le num° de tranche choisi est incorrect !");
                            }
                        }
                    }

                }
            }
            elseif (count($regls) == $nbF){
                foreach ($regls as $reglement) {
                    //   dd($regl);
                    $reglement->setStatusFrais('payé');
                    $this->entityManager->persist($entity);
                    $this->entityManager->flush();
                }
            }
            else {
                /*
//                dd("ok");
                // reglement de tranche déja fait
                $reglements = $this->entityManager->getRepository(Reglement::class)->findBy([
                    "frais" => $frChoisi,
                    "student" => $student,
                    "tranche" => $trChoisie,
                ]);
//            dd($reglements);

                if (count($reglements) > 1){
//                    dd($reglements[1]);
                    $this->entityManager->remove($reglements[1]);
                    $this->entityManager->flush();
                    $this->session->getFlashBag()->add('error', "le paiement de cette tranche est déja fait !");
                }
                */


//                $this->entityManager->remove($entity);
//                $this->entityManager->flush();
//                $this->session->getFlashBag()->add('error', "Toute les tranches de ce frais sont payés !");
            }
        }
    }








}