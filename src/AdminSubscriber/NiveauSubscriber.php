<?php


namespace App\AdminSubscriber;


use App\Entity\Niveau;
use App\Entity\AnneeScolaire;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class NiveauSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    private $session;

    /**
     * SeanceSubscriber constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager,  SessionInterface $session){
        $this->entityManager = $entityManager;
        $this->session = $session;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            AfterEntityPersistedEvent::class => ['newPeriode'],
        ];
    }


    /**
     * @param AfterEntityPersistedEvent $event
     */

    public function newPeriode(AfterEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if ($entity instanceof Niveau) {
            $lib = $entity->getLibelle();
            $annee = $entity->getAnneeScolaire();

            $niveaux = $this->entityManager->getRepository(\App\Entity\Niveau::class)->findBy([
                "libelle" => $lib,
                "anneeScolaire" => $annee,

            ]);
//            dd($seances);
            if (count($niveaux) > 1) {
                $this->entityManager->remove($entity);
                $this->entityManager->persist($entity);
                $this->entityManager->flush();
                $this->session->getFlashBag()->add('error', 'désolé ce niveau existe déja !!');
            }

            $nivs = $this->entityManager->getRepository(\App\Entity\AnneeScolaire::class)->findBy([
                "cloturee" => 1
            ]);
//            dd($seances);
            if (count($nivs) > 0 ) {
                $this->entityManager->remove($entity);
                $this->entityManager->flush();
                $this->session->getFlashBag()->add('error', "Désolé l'année sélectionnée est cloturée !!");
            }

        }

    }
}