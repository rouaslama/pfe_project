<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210622093011 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE absence (id INT AUTO_INCREMENT NOT NULL, seance_id INT DEFAULT NULL, professor_id INT DEFAULT NULL, type VARCHAR(255) NOT NULL, date DATETIME DEFAULT NULL, INDEX IDX_765AE0C9E3797A94 (seance_id), INDEX IDX_765AE0C97D2D84D5 (professor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE absence_student (absence_id INT NOT NULL, student_id INT NOT NULL, INDEX IDX_7EA58ABB2DFF238F (absence_id), INDEX IDX_7EA58ABBCB944F1A (student_id), PRIMARY KEY(absence_id, student_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE actualite (id INT AUTO_INCREMENT NOT NULL, titre VARCHAR(255) DEFAULT NULL, description VARCHAR(255) NOT NULL, fichier VARCHAR(255) DEFAULT NULL, date_creation DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE actualite_groupe (actualite_id INT NOT NULL, groupe_id INT NOT NULL, INDEX IDX_EDA008E8A2843073 (actualite_id), INDEX IDX_EDA008E87A45358C (groupe_id), PRIMARY KEY(actualite_id, groupe_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE actualite_professor (actualite_id INT NOT NULL, professor_id INT NOT NULL, INDEX IDX_DCF78F4AA2843073 (actualite_id), INDEX IDX_DCF78F4A7D2D84D5 (professor_id), PRIMARY KEY(actualite_id, professor_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE administrator (id INT NOT NULL, cin INT NOT NULL, UNIQUE INDEX UNIQ_58DF0651ABE530DA (cin), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE annee_scolaire (id INT AUTO_INCREMENT NOT NULL, debut_annee INT NOT NULL, fin_annee INT NOT NULL, libelle VARCHAR(255) NOT NULL, cloturee TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bulletin (id INT AUTO_INCREMENT NOT NULL, moyenne_g NUMERIC(4, 2) DEFAULT NULL, appreciation VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE devoir (id INT AUTO_INCREMENT NOT NULL, matiere_id INT NOT NULL, type_id INT NOT NULL, date DATE NOT NULL, h_debut TIME NOT NULL, h_fin TIME NOT NULL, INDEX IDX_749EA771F46CD258 (matiere_id), INDEX IDX_749EA771C54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE devoir_groupe (devoir_id INT NOT NULL, groupe_id INT NOT NULL, INDEX IDX_39E73FD1C583534E (devoir_id), INDEX IDX_39E73FD17A45358C (groupe_id), PRIMARY KEY(devoir_id, groupe_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE frais (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE frais_niveau (frais_id INT NOT NULL, niveau_id INT NOT NULL, INDEX IDX_D94EE1E1BF516DC4 (frais_id), INDEX IDX_D94EE1E1B3E9C81 (niveau_id), PRIMARY KEY(frais_id, niveau_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE groupe (id INT AUTO_INCREMENT NOT NULL, niveau_id INT NOT NULL, libelle VARCHAR(255) NOT NULL, nb_eleves INT DEFAULT NULL, nb_max INT NOT NULL, INDEX IDX_4B98C21B3E9C81 (niveau_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hygiene_officer (id INT NOT NULL, cin INT NOT NULL, UNIQUE INDEX UNIQ_B3667785ABE530DA (cin), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE matiere (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, coefficient NUMERIC(12, 2) DEFAULT \'0\' NOT NULL, moyenne_m DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE matiere_niveau (matiere_id INT NOT NULL, niveau_id INT NOT NULL, INDEX IDX_6B3CD676F46CD258 (matiere_id), INDEX IDX_6B3CD676B3E9C81 (niveau_id), PRIMARY KEY(matiere_id, niveau_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE message (id INT AUTO_INCREMENT NOT NULL, destination_id INT NOT NULL, sujet VARCHAR(255) DEFAULT NULL, contenu VARCHAR(255) DEFAULT NULL, date_creation DATETIME NOT NULL, INDEX IDX_B6BD307F816C6140 (destination_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE niveau (id INT AUTO_INCREMENT NOT NULL, annee_scolaire_id INT NOT NULL, libelle VARCHAR(255) NOT NULL, INDEX IDX_4BDFF36B9331C741 (annee_scolaire_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE note (id INT AUTO_INCREMENT NOT NULL, student_id INT DEFAULT NULL, periode_id INT NOT NULL, devoir_id INT NOT NULL, note DOUBLE PRECISION NOT NULL, appreciation VARCHAR(255) DEFAULT NULL, INDEX IDX_CFBDFA14CB944F1A (student_id), INDEX IDX_CFBDFA14F384C1CF (periode_id), INDEX IDX_CFBDFA14C583534E (devoir_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE periode (id INT AUTO_INCREMENT NOT NULL, annee_scolaire_id INT NOT NULL, num_periode INT NOT NULL, date_debut DATE DEFAULT NULL, date_fin DATE DEFAULT NULL, cloturee TINYINT(1) NOT NULL, moyenne_p DOUBLE PRECISION DEFAULT NULL, libelle VARCHAR(255) DEFAULT NULL, INDEX IDX_93C32DF39331C741 (annee_scolaire_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE periode_bulletin (periode_id INT NOT NULL, bulletin_id INT NOT NULL, INDEX IDX_AEB2A501F384C1CF (periode_id), INDEX IDX_AEB2A501D1AAB236 (bulletin_id), PRIMARY KEY(periode_id, bulletin_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE professor (id INT NOT NULL, cin INT NOT NULL, UNIQUE INDEX UNIQ_790DD7E3ABE530DA (cin), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE professor_matiere (professor_id INT NOT NULL, matiere_id INT NOT NULL, INDEX IDX_CF3B381E7D2D84D5 (professor_id), INDEX IDX_CF3B381EF46CD258 (matiere_id), PRIMARY KEY(professor_id, matiere_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE punition (id INT AUTO_INCREMENT NOT NULL, raison VARCHAR(255) NOT NULL, date_creation DATETIME NOT NULL, type VARCHAR(255) DEFAULT NULL, duree VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE punition_student (punition_id INT NOT NULL, student_id INT NOT NULL, INDEX IDX_D6F710AEB0879A41 (punition_id), INDEX IDX_D6F710AECB944F1A (student_id), PRIMARY KEY(punition_id, student_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reglement (id INT AUTO_INCREMENT NOT NULL, student_id INT NOT NULL, frais_id INT NOT NULL, tranche_id INT NOT NULL, mode_regl VARCHAR(255) DEFAULT NULL, status_frais VARCHAR(255) DEFAULT NULL, date_reglement DATETIME NOT NULL, INDEX IDX_EBE4C14CCB944F1A (student_id), INDEX IDX_EBE4C14CBF516DC4 (frais_id), INDEX IDX_EBE4C14CB76F6B31 (tranche_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE salle (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, capacite INT NOT NULL, UNIQUE INDEX UNIQ_4E977E5C6C6E55B5 (nom), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE seance (id INT AUTO_INCREMENT NOT NULL, professor_id INT NOT NULL, groupe_id INT NOT NULL, matiere_id INT NOT NULL, salle_id INT NOT NULL, jour VARCHAR(255) NOT NULL, num_seance INT NOT NULL, INDEX IDX_DF7DFD0E7D2D84D5 (professor_id), INDEX IDX_DF7DFD0E7A45358C (groupe_id), INDEX IDX_DF7DFD0EF46CD258 (matiere_id), INDEX IDX_DF7DFD0EDC304035 (salle_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE student (id INT NOT NULL, tutor_id INT NOT NULL, groupe_id INT DEFAULT NULL, INDEX IDX_B723AF33208F64F1 (tutor_id), INDEX IDX_B723AF337A45358C (groupe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tranche (id INT AUTO_INCREMENT NOT NULL, frais_id INT NOT NULL, montant NUMERIC(12, 3) DEFAULT \'0\' NOT NULL, num_tranche INT NOT NULL, INDEX IDX_66675840BF516DC4 (frais_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tutor (id INT NOT NULL, cin INT NOT NULL, profession VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_99074648ABE530DA (cin), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, coef NUMERIC(12, 2) DEFAULT \'0\' NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(255) DEFAULT NULL, user_name VARCHAR(255) DEFAULT NULL, password VARCHAR(255) DEFAULT NULL, roles LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, birthday DATE NOT NULL, gender VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, tel INT DEFAULT NULL, updated_at DATETIME NOT NULL, photo VARCHAR(255) DEFAULT NULL, type VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D64924A232CF (user_name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE absence ADD CONSTRAINT FK_765AE0C9E3797A94 FOREIGN KEY (seance_id) REFERENCES seance (id)');
        $this->addSql('ALTER TABLE absence ADD CONSTRAINT FK_765AE0C97D2D84D5 FOREIGN KEY (professor_id) REFERENCES professor (id)');
        $this->addSql('ALTER TABLE absence_student ADD CONSTRAINT FK_7EA58ABB2DFF238F FOREIGN KEY (absence_id) REFERENCES absence (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE absence_student ADD CONSTRAINT FK_7EA58ABBCB944F1A FOREIGN KEY (student_id) REFERENCES student (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE actualite_groupe ADD CONSTRAINT FK_EDA008E8A2843073 FOREIGN KEY (actualite_id) REFERENCES actualite (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE actualite_groupe ADD CONSTRAINT FK_EDA008E87A45358C FOREIGN KEY (groupe_id) REFERENCES groupe (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE actualite_professor ADD CONSTRAINT FK_DCF78F4AA2843073 FOREIGN KEY (actualite_id) REFERENCES actualite (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE actualite_professor ADD CONSTRAINT FK_DCF78F4A7D2D84D5 FOREIGN KEY (professor_id) REFERENCES professor (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE administrator ADD CONSTRAINT FK_58DF0651BF396750 FOREIGN KEY (id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE devoir ADD CONSTRAINT FK_749EA771F46CD258 FOREIGN KEY (matiere_id) REFERENCES matiere (id)');
        $this->addSql('ALTER TABLE devoir ADD CONSTRAINT FK_749EA771C54C8C93 FOREIGN KEY (type_id) REFERENCES type (id)');
        $this->addSql('ALTER TABLE devoir_groupe ADD CONSTRAINT FK_39E73FD1C583534E FOREIGN KEY (devoir_id) REFERENCES devoir (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE devoir_groupe ADD CONSTRAINT FK_39E73FD17A45358C FOREIGN KEY (groupe_id) REFERENCES groupe (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE frais_niveau ADD CONSTRAINT FK_D94EE1E1BF516DC4 FOREIGN KEY (frais_id) REFERENCES frais (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE frais_niveau ADD CONSTRAINT FK_D94EE1E1B3E9C81 FOREIGN KEY (niveau_id) REFERENCES niveau (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE groupe ADD CONSTRAINT FK_4B98C21B3E9C81 FOREIGN KEY (niveau_id) REFERENCES niveau (id)');
        $this->addSql('ALTER TABLE hygiene_officer ADD CONSTRAINT FK_B3667785BF396750 FOREIGN KEY (id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE matiere_niveau ADD CONSTRAINT FK_6B3CD676F46CD258 FOREIGN KEY (matiere_id) REFERENCES matiere (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE matiere_niveau ADD CONSTRAINT FK_6B3CD676B3E9C81 FOREIGN KEY (niveau_id) REFERENCES niveau (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F816C6140 FOREIGN KEY (destination_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE niveau ADD CONSTRAINT FK_4BDFF36B9331C741 FOREIGN KEY (annee_scolaire_id) REFERENCES annee_scolaire (id)');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT FK_CFBDFA14CB944F1A FOREIGN KEY (student_id) REFERENCES student (id)');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT FK_CFBDFA14F384C1CF FOREIGN KEY (periode_id) REFERENCES periode (id)');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT FK_CFBDFA14C583534E FOREIGN KEY (devoir_id) REFERENCES devoir (id)');
        $this->addSql('ALTER TABLE periode ADD CONSTRAINT FK_93C32DF39331C741 FOREIGN KEY (annee_scolaire_id) REFERENCES annee_scolaire (id)');
        $this->addSql('ALTER TABLE periode_bulletin ADD CONSTRAINT FK_AEB2A501F384C1CF FOREIGN KEY (periode_id) REFERENCES periode (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE periode_bulletin ADD CONSTRAINT FK_AEB2A501D1AAB236 FOREIGN KEY (bulletin_id) REFERENCES bulletin (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE professor ADD CONSTRAINT FK_790DD7E3BF396750 FOREIGN KEY (id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE professor_matiere ADD CONSTRAINT FK_CF3B381E7D2D84D5 FOREIGN KEY (professor_id) REFERENCES professor (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE professor_matiere ADD CONSTRAINT FK_CF3B381EF46CD258 FOREIGN KEY (matiere_id) REFERENCES matiere (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE punition_student ADD CONSTRAINT FK_D6F710AEB0879A41 FOREIGN KEY (punition_id) REFERENCES punition (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE punition_student ADD CONSTRAINT FK_D6F710AECB944F1A FOREIGN KEY (student_id) REFERENCES student (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reglement ADD CONSTRAINT FK_EBE4C14CCB944F1A FOREIGN KEY (student_id) REFERENCES student (id)');
        $this->addSql('ALTER TABLE reglement ADD CONSTRAINT FK_EBE4C14CBF516DC4 FOREIGN KEY (frais_id) REFERENCES frais (id)');
        $this->addSql('ALTER TABLE reglement ADD CONSTRAINT FK_EBE4C14CB76F6B31 FOREIGN KEY (tranche_id) REFERENCES tranche (id)');
        $this->addSql('ALTER TABLE seance ADD CONSTRAINT FK_DF7DFD0E7D2D84D5 FOREIGN KEY (professor_id) REFERENCES professor (id)');
        $this->addSql('ALTER TABLE seance ADD CONSTRAINT FK_DF7DFD0E7A45358C FOREIGN KEY (groupe_id) REFERENCES groupe (id)');
        $this->addSql('ALTER TABLE seance ADD CONSTRAINT FK_DF7DFD0EF46CD258 FOREIGN KEY (matiere_id) REFERENCES matiere (id)');
        $this->addSql('ALTER TABLE seance ADD CONSTRAINT FK_DF7DFD0EDC304035 FOREIGN KEY (salle_id) REFERENCES salle (id)');
        $this->addSql('ALTER TABLE student ADD CONSTRAINT FK_B723AF33208F64F1 FOREIGN KEY (tutor_id) REFERENCES tutor (id)');
        $this->addSql('ALTER TABLE student ADD CONSTRAINT FK_B723AF337A45358C FOREIGN KEY (groupe_id) REFERENCES groupe (id)');
        $this->addSql('ALTER TABLE student ADD CONSTRAINT FK_B723AF33BF396750 FOREIGN KEY (id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tranche ADD CONSTRAINT FK_66675840BF516DC4 FOREIGN KEY (frais_id) REFERENCES frais (id)');
        $this->addSql('ALTER TABLE tutor ADD CONSTRAINT FK_99074648BF396750 FOREIGN KEY (id) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE absence_student DROP FOREIGN KEY FK_7EA58ABB2DFF238F');
        $this->addSql('ALTER TABLE actualite_groupe DROP FOREIGN KEY FK_EDA008E8A2843073');
        $this->addSql('ALTER TABLE actualite_professor DROP FOREIGN KEY FK_DCF78F4AA2843073');
        $this->addSql('ALTER TABLE niveau DROP FOREIGN KEY FK_4BDFF36B9331C741');
        $this->addSql('ALTER TABLE periode DROP FOREIGN KEY FK_93C32DF39331C741');
        $this->addSql('ALTER TABLE periode_bulletin DROP FOREIGN KEY FK_AEB2A501D1AAB236');
        $this->addSql('ALTER TABLE devoir_groupe DROP FOREIGN KEY FK_39E73FD1C583534E');
        $this->addSql('ALTER TABLE note DROP FOREIGN KEY FK_CFBDFA14C583534E');
        $this->addSql('ALTER TABLE frais_niveau DROP FOREIGN KEY FK_D94EE1E1BF516DC4');
        $this->addSql('ALTER TABLE reglement DROP FOREIGN KEY FK_EBE4C14CBF516DC4');
        $this->addSql('ALTER TABLE tranche DROP FOREIGN KEY FK_66675840BF516DC4');
        $this->addSql('ALTER TABLE actualite_groupe DROP FOREIGN KEY FK_EDA008E87A45358C');
        $this->addSql('ALTER TABLE devoir_groupe DROP FOREIGN KEY FK_39E73FD17A45358C');
        $this->addSql('ALTER TABLE seance DROP FOREIGN KEY FK_DF7DFD0E7A45358C');
        $this->addSql('ALTER TABLE student DROP FOREIGN KEY FK_B723AF337A45358C');
        $this->addSql('ALTER TABLE devoir DROP FOREIGN KEY FK_749EA771F46CD258');
        $this->addSql('ALTER TABLE matiere_niveau DROP FOREIGN KEY FK_6B3CD676F46CD258');
        $this->addSql('ALTER TABLE professor_matiere DROP FOREIGN KEY FK_CF3B381EF46CD258');
        $this->addSql('ALTER TABLE seance DROP FOREIGN KEY FK_DF7DFD0EF46CD258');
        $this->addSql('ALTER TABLE frais_niveau DROP FOREIGN KEY FK_D94EE1E1B3E9C81');
        $this->addSql('ALTER TABLE groupe DROP FOREIGN KEY FK_4B98C21B3E9C81');
        $this->addSql('ALTER TABLE matiere_niveau DROP FOREIGN KEY FK_6B3CD676B3E9C81');
        $this->addSql('ALTER TABLE note DROP FOREIGN KEY FK_CFBDFA14F384C1CF');
        $this->addSql('ALTER TABLE periode_bulletin DROP FOREIGN KEY FK_AEB2A501F384C1CF');
        $this->addSql('ALTER TABLE absence DROP FOREIGN KEY FK_765AE0C97D2D84D5');
        $this->addSql('ALTER TABLE actualite_professor DROP FOREIGN KEY FK_DCF78F4A7D2D84D5');
        $this->addSql('ALTER TABLE professor_matiere DROP FOREIGN KEY FK_CF3B381E7D2D84D5');
        $this->addSql('ALTER TABLE seance DROP FOREIGN KEY FK_DF7DFD0E7D2D84D5');
        $this->addSql('ALTER TABLE punition_student DROP FOREIGN KEY FK_D6F710AEB0879A41');
        $this->addSql('ALTER TABLE seance DROP FOREIGN KEY FK_DF7DFD0EDC304035');
        $this->addSql('ALTER TABLE absence DROP FOREIGN KEY FK_765AE0C9E3797A94');
        $this->addSql('ALTER TABLE absence_student DROP FOREIGN KEY FK_7EA58ABBCB944F1A');
        $this->addSql('ALTER TABLE note DROP FOREIGN KEY FK_CFBDFA14CB944F1A');
        $this->addSql('ALTER TABLE punition_student DROP FOREIGN KEY FK_D6F710AECB944F1A');
        $this->addSql('ALTER TABLE reglement DROP FOREIGN KEY FK_EBE4C14CCB944F1A');
        $this->addSql('ALTER TABLE reglement DROP FOREIGN KEY FK_EBE4C14CB76F6B31');
        $this->addSql('ALTER TABLE student DROP FOREIGN KEY FK_B723AF33208F64F1');
        $this->addSql('ALTER TABLE devoir DROP FOREIGN KEY FK_749EA771C54C8C93');
        $this->addSql('ALTER TABLE administrator DROP FOREIGN KEY FK_58DF0651BF396750');
        $this->addSql('ALTER TABLE hygiene_officer DROP FOREIGN KEY FK_B3667785BF396750');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F816C6140');
        $this->addSql('ALTER TABLE professor DROP FOREIGN KEY FK_790DD7E3BF396750');
        $this->addSql('ALTER TABLE student DROP FOREIGN KEY FK_B723AF33BF396750');
        $this->addSql('ALTER TABLE tutor DROP FOREIGN KEY FK_99074648BF396750');
        $this->addSql('DROP TABLE absence');
        $this->addSql('DROP TABLE absence_student');
        $this->addSql('DROP TABLE actualite');
        $this->addSql('DROP TABLE actualite_groupe');
        $this->addSql('DROP TABLE actualite_professor');
        $this->addSql('DROP TABLE administrator');
        $this->addSql('DROP TABLE annee_scolaire');
        $this->addSql('DROP TABLE bulletin');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE devoir');
        $this->addSql('DROP TABLE devoir_groupe');
        $this->addSql('DROP TABLE frais');
        $this->addSql('DROP TABLE frais_niveau');
        $this->addSql('DROP TABLE groupe');
        $this->addSql('DROP TABLE hygiene_officer');
        $this->addSql('DROP TABLE matiere');
        $this->addSql('DROP TABLE matiere_niveau');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE niveau');
        $this->addSql('DROP TABLE note');
        $this->addSql('DROP TABLE periode');
        $this->addSql('DROP TABLE periode_bulletin');
        $this->addSql('DROP TABLE professor');
        $this->addSql('DROP TABLE professor_matiere');
        $this->addSql('DROP TABLE punition');
        $this->addSql('DROP TABLE punition_student');
        $this->addSql('DROP TABLE reglement');
        $this->addSql('DROP TABLE salle');
        $this->addSql('DROP TABLE seance');
        $this->addSql('DROP TABLE student');
        $this->addSql('DROP TABLE tranche');
        $this->addSql('DROP TABLE tutor');
        $this->addSql('DROP TABLE type');
        $this->addSql('DROP TABLE user');
    }
}
